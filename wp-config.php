<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'customer_yachtsman');

/** MySQL database username */
define('DB_USER', 'customer_yachts');

/** MySQL database password */
define('DB_PASSWORD', 'im@rk123#@');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9yjWS9+t#+[<vOkzd~h}$h)USd7cG=Z`B+_|SSVQ_Yc]3h#YsBcY49_vv21)_VWX');
define('SECURE_AUTH_KEY',  'cE#XZ2+[<XA%FI][`w)Q^5MQoEtmb5%70P,s=TPvo$/G0uC& v^P(D2~Y5)arY7c');
define('LOGGED_IN_KEY',    'Ic&,Eo$<f]Gj5h.EYl8egQ]lK_Ruv;jPw]C|1~bG/QFMkqG)Mh}>x[DC^nuf}<sx');
define('NONCE_KEY',        '$(-2)22`}aw_ccBx?x%IMdN^{ru,zDiyYx&:V1*>%e_8l8[3;OYi5^8)SG[BluqU');
define('AUTH_SALT',        'UA7tEv60/R,r~Jlo/~3RVjNVG@R* 4#k^; %pqEtZca/v`bD`OSRzz+%%;;[qf_F');
define('SECURE_AUTH_SALT', 'kbjF^GJJ$[=5F.r<y2=W)xXnp#=LVIX%X!4a62V(|Ve%.a+}UBLQbUR!RV<C(A4R');
define('LOGGED_IN_SALT',   '#zI[4vk5xg-CCD.Qa/8PSb&{5Ly-$-p*U;`gYfgVJ#2F5C^&piJ1@*UXyg5s>n -');
define('NONCE_SALT',       'Aev^2r,B2FfjU8VVK+`HQNG|ald.>D9H-VkuroTv?q>NdcH=c4r:*;8<BJybm}DJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
