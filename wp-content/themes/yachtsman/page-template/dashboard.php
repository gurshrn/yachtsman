
<?php 
/*
Template Name: dashboard
*/
get_header();
?>
<section class="dashboard_menu">
	<div id="sidebar-main" class="sidebar sidebar-default sidebar-separate sidebar-fixed">
		<div class="sidebar-content">
			<div class="sidebar-category sidebar-default">
				<div class="sidebar-user">
					<div class="category-content">
						<h6>John Doe</h6>
						<small>Admin</small>
					</div>
				</div>
			</div>
			<!-- /Sidebar Category -->
			<div class="sidebar-category sidebar-default">
				<div class="category-title">
					<span>Fruits</span>
				</div>
				<div class="category-content">
					<ul id="fruits-nav" class="nav flex-column">
						<li class="nav-item">
							<a href="#" class="nav-link">
								<i class="fa fa-pencil" aria-hidden="true"></i>
								Apple
							</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link">
								<i class="fa fa-pencil" aria-hidden="true"></i>
								Banana
							</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link active">
								<i class="fa fa-pencil" aria-hidden="true"></i>
								Pear
							</a>
						</li>
						<li class="nav-item">
							<a href="#other-fruits" class="nav-link" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="other-fruits">
								<i class="fa fa-pencil" aria-hidden="true"></i>
								Others
							</a>
							<ul id="other-fruits" class="flex-column collapse">
								<li class="nav-item">
									<a href="#" class="nav-link">
										<i class="fa fa-pencil" aria-hidden="true"></i>
										Orange
									</a>
								</li>
								<li class="nav-item ">
									<a href="#" class="nav-link">
										<i class="fa fa-pencil" aria-hidden="true"></i>
										Kiwi
									</a>
								</li>
							</ul>
							<!-- /Sub Nav -->
						</li>
					</ul>
					<!-- /Nav -->
				</div>
				<!-- /Category Content -->
			</div>
			<!-- /Sidebar Category -->
		</div>
	</div>
	<!-- /Right SideBar Category -->
	<div class="sidebar_right">
		<h4>Demo text</h4>
	</div>

</section>