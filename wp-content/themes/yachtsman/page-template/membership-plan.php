<?php
/*
Template Name: membership-plans
*/
get_header();
get_sidebar();

$bannerImage = get_field('banner_image');

if(isset($_POST) && !empty($_POST))
{
	
	add_user_detail($_POST);
}

?>

        <section class="inner banner" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">
                <div class="banner_content">

                    <h2><?php the_field('banner_text');?></h2>

                </div>
            </div>
        </section>
        <section class="membership membership-plan">
            <div class="container">

                <h4><strong><?php the_field('membership_title');?></strong></h4>

                <div class="row">
                    <?php
                        $args = array(
                          'post_type'   => 'membership-plan',
                          'post_status' => 'publish',
                          'order' => 'ASC',
                        );
                        $products = new WP_Query( $args );

                        if( $products->have_posts() ) :

                            while( $products->have_posts() ) :
                                $products->the_post();
                    ?>
                            <div class="col-sm-4 col-12 wow fadeIn" data-wow-delay="0.2s">
                                <div class="pricingTable">
                                    <div class="pricingTable-header">
                                        <div class="priceTable_top">

                                            <p><?php echo  get_the_title(); ?></p>

                                            <span class="currency">$</span>
                                            <h3 class="title">100</h3>
                                            <div class="dollor">
                                                <span class="price-value">**</span>
                                                <span class="month">/ Per Month</span>
                                            </div>
                                        </div>
                                        <div class="pricing-middle">
                                            <h4>Benefits and Features</h4>
                                            <?php
                                                if( have_rows('plan_feature', $post->ID) ):

                                                $i=0;

                                                while( have_rows('plan_feature', $post->ID) ): the_row();

                                            ?>
                                                    <p>
                                                        <?php
                                                            if($i != 0)
                                                            {
                                                                echo '+';
                                                            }
                                                            the_sub_field('feature_title');
                                                        ?>

                                                    </p>

                                                <?php
                                                    $i++;
                                                    endwhile;
                                                    endif;
                                                ?>

                                        </div>
                                    </div>
                                    <div class="pricing-content">
                                        <ul>
                                            <?php
                                                if( have_rows('plan_detail', $post->ID) ):

                                                $i=1;

                                                while( have_rows('plan_detail', $post->ID) ): the_row();

                                                    $addCount = get_sub_field('add_count');
                                                    $currency = get_sub_field('currency');
                                                    $monthly_price = get_sub_field('monthly_price');
                                                    $yearly_price = get_sub_field('yearly_price');
                                            ?>
                                                    <li>
                                                        <div class="form-group">



                                                            <input type="radio" name="subscription_plan_price" id="<?php echo get_the_title().'_'.$i;?>" data-limit="<?php echo $addCount;?>" data-yearly-price="<?php echo $yearly_price;?>" data-monthly-price="<?php echo $monthly_price;?>" value="<?php echo get_the_title();?>">

                                                            <label for="<?php echo get_the_title().'_'.$i;?>">
                                                                <?php
                                                                    echo $addCount.' '.'@'.' '.$currency.' '.$monthly_price.'/month or'.' '.$currency.' '.$yearly_price.'/year';
                                                                ?>
                                                            </label>
                                                        </div>
                                                    </li>
                                            <?php
                                                $i++;
                                                endwhile;
                                                wp_reset_postdata();
                                                endif;
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="button">
                                        <a class="btn-effect subscribe-now" href="javascript:void(0);"><span>Subscribe Now</span></a>
                                    </div>
                                </div>
                            </div>

                    <?php
                        endwhile;
                        wp_reset_postdata();
                        endif;
                    ?>

                    <div class="col-sm-4 col-12  wow fadeIn" data-wow-delay="0.6s">
                        <div class="membership_form">
                            <div class="red_carpet">
                                <h5>red carpet - concierge</h5>
                                <p>
                                    **NEW** Don’t like Computers or too busy?? Hire a live professional to physically
                                    sell or shop your next vessel $ USD
                                    Hourly Rate on Request
                                </p>
                                <span class="triangle-top-left"></span>
                                <span class="triangle-top-right"></span>
                                <span class="triangle-bottom-left"></span>
                                <span class="triangle-bottom-right"></span>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input class="form-control firstCap" type="text" name="name" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="select_area">
                                                <div class="city">
                                                    <select class="form-control">
                                                        <option>City</option>
                                                        <option>United States</option>
                                                        <option>English (asd)</option>
                                                    </select>
                                                </div>
                                                <div class="State">
                                                    <select class="form-control">
                                                        <option>State</option>
                                                        <option>United States</option>
                                                        <option>English (asd)</option>
                                                    </select>
                                                </div>
                                                <div class="country">
                                                    <select class="form-control">
                                                        <option>Country</option>
                                                        <option>United States</option>
                                                        <option>English (asd)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="mail" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="phone" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea class="form-control firstCap" placeholder="Details"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="button-submit">
                                            <button class="btn_sbmit" type="submit">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-sm-12  wow fadeIn" data-wow-delay="0.2s">
                        <div class="membership_bottom">
                            <p>
                                I need to know when someone signs up for this, so I can appoint a media marketing
                                expert to
                                the client
                            </p>
                            <h5>NEW: Hire a live person over the phone to physically shop your next boat</h5>
                            <div class="member_ads">
                                <figure>
                                    <span class="thumbnail_wrap image">
                                        <span><img alt="" class="thumbnail" data-attachment-id="347427815"
                                                data-audio-codec="null" data-classes="image" data-container-id="comment_647386854"
                                                data-content-type="image/gif" data-created-at="2018-09-10T14:00:08Z"
                                                data-creator-id="16742695" data-creator="John R." data-description="image"
                                                data-details-path="/2175374/projects/15787039/attachments/347427815/details"
                                                data-download-path="/2175374/projects/15787039/attachments/347427815/download"
                                                data-embeddable="false" data-extension="GIF" data-file-or-image="image"
                                                data-filename="WWY-Header Ad for Adwords Remarketing.gif" data-filesize="172 KB"
                                                data-height="60" data-image-id="347427815" data-large-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427815/b3da1f8e-b501-11e8-97fe-047d7badf11c/large.gif"
                                                data-linked="null" data-max-size="700" data-original-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427815"
                                                data-path="/2175374/projects/15787039/attachments/347427815"
                                                data-perma-path="/2175374/projects/15787039/messages/80428610?enlarge=347427815#attachment_347427815"
                                                data-previewable="true" data-storage-key="b3da1f8e-b501-11e8-97fe-047d7badf11c"
                                                data-thumbnail="true" data-trash-path="/2175374/projects/15787039/attachments/347427815/trash"
                                                data-trashed="false" data-type="image" data-video-codec="null"
                                                data-width="468" src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427815/b3da1f8e-b501-11e8-97fe-047d7badf11c/thumbnail.gif"
                                                data-scaled="true" style="width: 468px; height: 60px;"></span>
                                    </span>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!--Subscribe Modal-->

        <div id="subscribe-plan" class="modal login_popup fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Subscription</h4>
              </div>
              <div class="modal-body" id="subscription-forms">
                <div class="login  subscription-forms">

                    <form method="POST" action="<?php echo get_site_url().'/membership-plans/'; ?>" id="subscription-form">


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" type="text" name="user_name" placeholder="Name" required>

                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" type="email" name="user_email" placeholder="Email" required>

                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="plan" name="boat_plan" placeholder="Plan" readonly>
                                </div>
                            </div>
                             <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="boat_limit" name="boat_limit" placeholder="Boat Limit" readonly>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="hidden" name="plan_type" id="plan_type">
                                    <div class="radio">
                                        <input id="yearly_price" class="boat-price" data-attr="Yearly" name="plan_type" type="radio">
                                        <label for="yearly_price" class="radio-label">Yearly</label>
                                    </div>
                                     <div class="radio">
                                        <input id="monthly_price" class="boat-price" data-attr="Monthly" name="plan_type" type="radio">
                                        <label for="monthly_price" class="radio-label">Monthly</label>
                                    </div>
                                    <!-- <input class="form-control" type="radio" id="yearly_price" name="plan_type">
                                    <input class="form-control" type="radio" id="monthly_price" name="mail"> -->

                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="plan_price" placeholder="Price" id="boat_price" readonly>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="password" name="user_password" placeholder="Password" required>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                 <div class="submit ">
                                    <button class="btn_sbmit submit-subscription" type="submit">Submit</button>

                                </div>
                            </div>
                        </div>
                    </form>

                    </div>
              </div>

              <div class="modal-body" id="text-data" style="display:none;"></div>
            </div>

          </div>
        </div>

<?php get_footer(); ?>

<script>
    jQuery(document).on('click','.subscribe-now',function(){
        //alert('test');
        if(jQuery("input:radio[name='subscription_plan_price']").is(":checked"))
        {
            jQuery('#subscribe-plan').modal('show');
            jQuery('#subscription-forms').css('display','block');
            jQuery('#text-data').css('display','none');

            var plan = jQuery('input[name=subscription_plan_price]:checked').val();
            var boat_limit = jQuery('input[name=subscription_plan_price]:checked').attr('data-limit');
            var yearly_price = jQuery('input[name=subscription_plan_price]:checked').attr('data-yearly-price');
            var monthly_price = jQuery('input[name=subscription_plan_price]:checked').attr('data-monthly-price');


            jQuery('#plan').val(plan);
            jQuery('#boat_limit').val(boat_limit);
            jQuery('#yearly_price').val(yearly_price);
            jQuery('#monthly_price').val(monthly_price);


        }
        else
        {
            jQuery('#subscription-forms').css('display','none');
            jQuery('#text-data').css('display','block');
            jQuery('#text-data').html('Kindly select at least one plan option');
            jQuery('#subscribe-plan').modal('show');
        }

    });

    jQuery(document).on('change','.boat-price',function(){
        var boatPrice = jQuery(this).val();
        jQuery('#boat_price').val(boatPrice);
        var planType = jQuery(this).attr('data-attr');
        jQuery('#plan_type').val(planType);

    });




</script>