<?php 
/*
Template Name: boat-form
*/
?>

<section class="boat_sale wow fadeIn" data-wow-delay="0.3s">
    <div class="container">
        <div class="boat_content">
            <h3>Boats For Sale</h3>
            <form>
                <div class="manufacturer">
                    <div class="select_modal">
                        <div class="form-group">
                            <label>Manufacturer/model</label>
                            <input class="form-control" type="text" name="modal">
                        </div>
                    </div>
                    <div class="new_type">
                        <div class="form-group">
                            <label>New/Used</label>
                            <p> <input type="radio" id="rd_1" name="radio-group" checked>
                                <label for="rd_1">All</label>
                            </p>
                            <p>
                                <input type="radio" id="rd_2" name="radio-group">
                                <label for="rd_2">New</label>
                            </p>
                            <p>
                                <input type="radio" id="rd_3" name="radio-group">
                                <label for="rd_3">Used</label>
                            </p>
                        </div>
                    </div>
                    <div class="used_type">
                        <div class="form-group">
                            <label>Boat Type</label>
                            <p> <input type="radio" id="rd_4" name="radio-group-1" checked>
                                <label for="rd_4">All</label>
                            </p>
                            <p>
                                <input type="radio" id="rd_5" name="radio-group-1">
                                <label for="rd_5">Power</label>
                            </p>
                            <p>
                                <input type="radio" id="rd_6" name="radio-group-1">
                                <label for="rd_6">Soil</label>
                            </p>
                        </div>
                    </div>
                    <div class="lenght">
                        <div class="form-group">
                            <label>Length</label>
                            <p> <input type="radio" id="rd_7" name="radio-group-2" checked>
                                <label for="rd_7">Ft</label>
                            </p>
                            <p>
                                <input type="radio" id="rd_8" name="radio-group-2">
                                <label for="rd_8">M</label>
                            </p>
                            <input class="form-control" type="text" name="to">
                            <span>To</span>
                            <input class="form-control" type="text" name="send">
                        </div>
                    </div>
                </div>
                <div class="year_modal">
                    <div class="year">
                        <div class="form-group">
                            <label>Year</label>
                            <div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                                <input class="form-control" type="text" readonly />
                                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            </div>
                            <span>To</span>
                            <div id="datepicker-2" class="input-group date" data-date-format="mm-dd-yyyy">
                                <input class="form-control" type="text" readonly />
                                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="price">
                        <div class="form-group">
                            <label>Price USD</label>
                            <select class="form-control">
                                <option>Select Price Range</option>
                                <option>Select Price Range</option>
                                <option>Select Price Range</option>
                            </select>
                            <button class="boat_btn" type="submit">Search</button>
                        </div>
                    </div>
                    <div class="advance_search">
                        <a href="#">Advanced Search</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>