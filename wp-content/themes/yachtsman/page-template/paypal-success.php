<?php 
/*
Template Name: paypal-success
*/
global $wpdb;
session_start();

get_header();
get_sidebar();

if(!empty($_POST))
{
	$wpdb->update('wp_membership_plan', array('txn_id' => $_POST['txn_id'],
	'ipn_track_id' => $_POST['ipn_track_id'], 'payer_email' => $_POST['payer_email'],'order_date' => date('Y-m-d h:i:s'),'status' => $_POST['payment_status']), array('id' => $_POST['item_number']));
	
	$_SESSION['user_data'] = array('userId'=>$_POST['custom'],'userRole' =>$_POST['first_name']);
	
}

print_r($_SESSION);

	
?>

        <section class="inner banner" style="background-image: url(images/inner-banner.jpg)">
            <div class="container">
                <div class="banner_content">
                    <h2>Membership</h2>
                </div>
            </div>
        </section>
        <section class="membership two">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7 col-12 member">
                        <div class="membership_2">
                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                            <p>
                                Your Request has been sent to Admin
                                Your will Receive a Confirmation Shortly !
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-5 col-12 member-right">
                        <div class="ship_right">
                            <h4>Download brochure</h4>
                            <figure>
                                <img src="images/member-right.jpg" alt="Member">
                            </figure>
                            <div class="button">
                                <a class="btn-effect" href="#"><span>Download</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php get_footer();?>