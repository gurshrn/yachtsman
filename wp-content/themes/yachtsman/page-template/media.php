<?php 
/*
Template Name: media
*/
get_header();
get_sidebar();

?>
        <?php $bannerImage = get_field('media_banner_image');?>

        <section class="inner banner" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">
                <div class="banner_content">
                    <h2><?php echo the_field('media_banner_text');?></h2>
                </div>
            </div>
        </section>
        <section class="cookies">
            <div class="container">

                <?php the_field('media_content');?>

                <div class="sure">

                    <h4><?php the_field('media_link_title');?></h4>

                    <?php if( have_rows('media_link_content') ): ?>

                        <ul>

                            <?php while( have_rows('media_link_content') ): the_row(); 

                                $alphabet = get_sub_field('alphabet');
                                $title = get_sub_field('title');
                                $link = get_sub_field('title_link');
                               
                            ?>
                                <li>
                                    <span><?php echo $alphabet;?></span>
                                    <a href="<?php echo $link;?>"><?php echo $title;?></a>
                                </li>
                            
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>

                </div>
            </div>
        </section>

<?php get_footer();?>