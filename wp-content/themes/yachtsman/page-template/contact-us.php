<?php 
/*
Template Name: contact-us
*/
get_header();
get_sidebar();

?>
        <?php $bannerImage = get_field('contact_banner_image');?>

        <section class="inner banner" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">
                <div class="banner_content">
                    <h2><?php the_field('contact_banner_text');?></h2>
                </div>
            </div>
        </section>
        <section class="contact">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-12">
                        <div class="contact_left">

                            <?php the_field('contact_us_description');?>
                            
                        </div>
                    </div>
                    <div class="col-sm-6 col-12">
                        <div class="contact_right">

                            <h3><?php the_field('contact_form_title');?></h3>
                            
                            <?php echo do_shortcode( '[contact-form-7 id="44" title="Contact form"]' ); ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php get_footer(); ?>