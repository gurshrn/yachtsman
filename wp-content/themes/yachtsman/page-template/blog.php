<?php 
/*
Template Name: blog
*/
get_header();
get_sidebar();

?>

		<?php $bannerImage = get_field('blog_banner_image');?>
		
        <section class="inner banner" style="background-image: url(<?php echo $bannerImage['url'];?>)">
            <div class="container">
                <div class="banner_content">
                    <h2><?php the_field('blog_banner_text');?></h2>
                </div>
            </div>
        </section>
        <section class="blog">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 col-12">
                        <div class="blog_left">
                            <div class="blog_content">
                                <?php the_field('blog_content');?>
                            </div>
                            <div class="blog_img">
							
								<?php
								$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
									$args = array(
										'post_type' => 'post',
										'posts_per_page' => 1,
										'paged' => $paged,
									);

									$post_query = new WP_Query($args);
									if($post_query->have_posts() ){
									
										while($post_query->have_posts() ):
									  
											$post_query->the_post();
								?>
								
									<figure style="background-image: url(<?php echo the_post_thumbnail_url();?>)"></figure>
									<div class="blog_bottom">
										<div class="year">
											<p><strong><?php echo get_the_date('d'); ?></strong><?php echo get_the_date('M'); ?></p>
										</div>
										<div class="bottom_right">
											<h4><?php the_title(); ?></h4>
											<p><?php echo wordwrap(the_content(), 20); ?></p>
											<div class="features_social">
												<a href="<?php the_permalink(); ?>">Read More</a>
												<ul>
													<li> <a href="#"><i class="fa fa-facebook" aria-hidden="true" target="blank"></i></a></li>
													<li><a href="#"><i class="fa fa-twitter" aria-hidden="true" target="blank"></i></a></li>
													<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true" target="blank"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									
									<?php  endwhile;  ?>
								
                            </div>
                            <nav aria-label="...">
                                
								
								<?php //next_posts_link( 'Older Entries', $post_query->max_num_pages );
								
								pagination($post_query->max_num_pages);


								} ?>
                                    <!--li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1">1</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">4</a></li-->
                                
                            </nav>
                        </div>
                    </div>
                    <div class="col-sm-3 col-12">
                        <div class="blog_right">
                            <form>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="search" placeholder="Search...">
                                    <i class="fa fa-search"></i>
                                </div>
                            </form>
                            <div class="recent_post">
                                <h4>Recent Articles</h4>
                                <ul>
                                    <li><a href="#">May there always be water under your boat, </a></li>
                                    <li>
                                        <a href="#">May she always be seaworthy,ever afloat, </a>
                                    </li>
                                    <li>
                                        <a href="#">May the bilge pump be certain to worknight and day, </a>
                                    </li>
                                    <li>
                                        <a href="#">May the compass and charts always
                                            show the safe way, </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<?php get_footer();?>