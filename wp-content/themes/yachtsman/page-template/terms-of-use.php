<?php 
/*
Template Name: term-of-use
*/
get_header();
get_sidebar();

?>
        <?php $bannerImage = get_field('term_banner_image');?>

        <section class="inner banner" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">
                <div class="banner_content">
                    <h2><?php the_field('term_banner_text');?></h2>
                </div>
            </div>
        </section>
        <section class="terms-of-use">
            <div class="container">
                
                <h4><?php the_field('term_title');?></h4>
                
                <?php the_field('term_content');?>

            </div>
        </section>

<?php get_footer(); ?>