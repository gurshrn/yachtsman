<?php 

/*
Template Name: yacht-charters
*/
get_header();

$data = $_POST;
foreach($data as $key=>$val)
{
    if(!empty($val))
    {
        $metaQuery[] = array('key'=> $key,'value'   => $val);
    }
}

if(count($metaQuery) > 1)
{
    $metaQuery['relation']='OR';
} 


//echo "<pre>";print_r($query);

?>
        <?php $bannerImage = get_field('yacht_banner_image');?>
        
        <section class="inner banner" style="background-image: url(<?php echo $bannerImage['url'];?>)">
            
            <div class="container">
                <div class="banner_content">
                    
                    <h2><?php the_field('yacht_banner_text');?></h2>

                </div>
            </div>

        </section>
        <section class="world_wide">
            <div class="container">
                <div class="row">

                    <?php 
                        $args = array(
                            'post_type'  => 'boat',
                            'meta_query' => $metaQuery,
                        );
                        $query = new WP_Query( $args );
                        if( $query->have_posts() ) :
                                    
                            while( $query->have_posts() ) :
                                
                                $query->the_post();

                    ?>
                            <div class="col-sm-4 col-12 wow fadeIn" data-wow-delay="0.2s">
                                <div class="wide">
                                    <figure style="background-image: url(<?php echo the_post_thumbnail_url();?>)"></figure>
                                    <div class="wide_range">
                                        <div class="wide_left">
                                            
                                            <h4><?php the_title();?></h4>
                                            
                                            <p>
                                                <?php
                                                    $string = get_field('countries');
                                                    echo ucwords(str_replace("_"," ",$string));
                                                ?>
                                                    
                                            </p>

                                            <div class="button">
                                                <a class="btn-effect" href="<?php the_permalink();?>"><span>Download</span></a>
                                            </div>
                                        </div>
                                        <div class="wide_img">
                                            <figure>
                                                <img src="images/wide.png" alt="Wide">
                                            </figure>
                                            <h5>John</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php 

                        endwhile;
                        wp_reset_postdata();
                        endif;
                    ?> 
                </div>
            </div>
        </section>
<?php get_footer();?>