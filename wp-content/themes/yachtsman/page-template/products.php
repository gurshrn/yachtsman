<?php 
/*
Template Name: products
*/
get_header();
get_sidebar();

$data = $_POST;

$searchData = globalSearch($data);

?>

        <section class="inner banner" style="background-image: url(images/inner-banner.jpg)">
            <div class="container">
                <div class="banner_content">
                    <h2>Product</h2>
                </div>
            </div>
        </section>
        <section class="products">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-12 product_left">
                        <div class="product_fillter">
                            <h4>Filter</h4>
                            <form>
                                <div class="form-group">
                                    <label>Manufacturer /model</label>
                                    <input class="form-control" type="text" name="name" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>New/Used</label>
                                    <p> <input type="radio" id="rd_1" name="radio-group" checked>
                                        <label for="rd_1">All</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="rd_2" name="radio-group">
                                        <label for="rd_2">New</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="rd_3" name="radio-group">
                                        <label for="rd_3">Used</label>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <label>New/Used</label>
                                    <p> <input type="radio" id="rd_11" name="radio-group-2" checked>
                                        <label for="rd_11">All</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="rd_22" name="radio-group-2">
                                        <label for="rd_22">New</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="rd_33" name="radio-group-2">
                                        <label for="rd_33">Used</label>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <label>Price USD</label>
                                    <select class="form-control">
                                        <option>Select Price Range</option>
                                        <option>Select Price Range</option>
                                        <option>Select Price Range</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Year</label>
                                    <div class="date" data-provide="datepicker">
                                        <input type="text" class="form-control" placeholder="Select Year">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-6 col-12 product_center">

                        <?php 
                            if( !empty($searchData) ) :
                                        
                                foreach( $searchData as $posts ) :
                                    
                                    $url = get_the_post_thumbnail_url($posts->ID);
                                    $link = get_post_permalink($posts->ID);


                        ?>

                                    <div class="product_list">
                                        <div class="product_img">
                                            <figure style="background-image: url(<?php echo $url;?>)"> </figure>
                                        </div>
                                        <div class="product_content">

                                            <h4><?php echo $posts->post_title;?></h4>

                                            <strong><?php the_field('price',$posts->ID);?>**</strong>

                                            <p>

                                                <?php
                                                    $string = get_field('countries',$posts->ID);
                                                    echo ucwords(str_replace("_"," ",$string));
                                                ?>
                                                
                                            </p>
                                            
                                            <div class="button">
                                                <a class="btn-effect" href="<?php echo $link; ?>"><span>View Details</span></a>
                                            </div>
                                        </div>
                                    </div>

                        <?php 

                            endforeach;
                            wp_reset_postdata();
                            endif;
                        ?>
                        
                        <div class="load_more">
                            <a class="load" href="#">Load More</a>
                        </div>

                    </div>
                    <div class="col-sm-3 col-12 product_right">
                        <div class="product_ads">
                            <figure>
                                <span class="thumbnail_wrap image">
                                    <span><img alt="" class="thumbnail" data-attachment-id="347427814" data-audio-codec="null"
                                            data-classes="image" data-container-id="comment_647386854"
                                            data-content-type="image/gif" data-created-at="2018-09-10T14:00:08Z"
                                            data-creator-id="16742695" data-creator="John R." data-description="image"
                                            data-details-path="/2175374/projects/15787039/attachments/347427814/details"
                                            data-download-path="/2175374/projects/15787039/attachments/347427814/download"
                                            data-embeddable="false" data-extension="GIF" data-file-or-image="image"
                                            data-filename="WWY-Margin Ad for Adwords Remarketing.gif" data-filesize="153 KB"
                                            data-height="280" data-image-id="347427814" data-large-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/large.gif"
                                            data-linked="null" data-max-size="700" data-original-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814"
                                            data-path="/2175374/projects/15787039/attachments/347427814"
                                            data-perma-path="/2175374/projects/15787039/messages/80428610?enlarge=347427814#attachment_347427814"
                                            data-previewable="true" data-storage-key="b2d42efe-b501-11e8-bb59-047d7badf251"
                                            data-thumbnail="true" data-trash-path="/2175374/projects/15787039/attachments/347427814/trash"
                                            data-trashed="false" data-type="image" data-video-codec="null" data-width="336"
                                            src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/thumbnail.gif"
                                            data-scaled="true" style="width: 336px; height: 280px;"></span>
                                </span>
                            </figure>
                            <figure>
                                <span class="thumbnail_wrap image">
                                    <span><img alt="" class="thumbnail" data-attachment-id="347427814" data-audio-codec="null"
                                            data-classes="image" data-container-id="comment_647386854"
                                            data-content-type="image/gif" data-created-at="2018-09-10T14:00:08Z"
                                            data-creator-id="16742695" data-creator="John R." data-description="image"
                                            data-details-path="/2175374/projects/15787039/attachments/347427814/details"
                                            data-download-path="/2175374/projects/15787039/attachments/347427814/download"
                                            data-embeddable="false" data-extension="GIF" data-file-or-image="image"
                                            data-filename="WWY-Margin Ad for Adwords Remarketing.gif" data-filesize="153 KB"
                                            data-height="280" data-image-id="347427814" data-large-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/large.gif"
                                            data-linked="null" data-max-size="700" data-original-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814"
                                            data-path="/2175374/projects/15787039/attachments/347427814"
                                            data-perma-path="/2175374/projects/15787039/messages/80428610?enlarge=347427814#attachment_347427814"
                                            data-previewable="true" data-storage-key="b2d42efe-b501-11e8-bb59-047d7badf251"
                                            data-thumbnail="true" data-trash-path="/2175374/projects/15787039/attachments/347427814/trash"
                                            data-trashed="false" data-type="image" data-video-codec="null" data-width="336"
                                            src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/thumbnail.gif"
                                            data-scaled="true" style="width: 336px; height: 280px;"></span>
                                </span>
                            </figure>
                            <figure>
                                <span class="thumbnail_wrap image">
                                    <span><img alt="" class="thumbnail" data-attachment-id="347427814" data-audio-codec="null"
                                            data-classes="image" data-container-id="comment_647386854"
                                            data-content-type="image/gif" data-created-at="2018-09-10T14:00:08Z"
                                            data-creator-id="16742695" data-creator="John R." data-description="image"
                                            data-details-path="/2175374/projects/15787039/attachments/347427814/details"
                                            data-download-path="/2175374/projects/15787039/attachments/347427814/download"
                                            data-embeddable="false" data-extension="GIF" data-file-or-image="image"
                                            data-filename="WWY-Margin Ad for Adwords Remarketing.gif" data-filesize="153 KB"
                                            data-height="280" data-image-id="347427814" data-large-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/large.gif"
                                            data-linked="null" data-max-size="700" data-original-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814"
                                            data-path="/2175374/projects/15787039/attachments/347427814"
                                            data-perma-path="/2175374/projects/15787039/messages/80428610?enlarge=347427814#attachment_347427814"
                                            data-previewable="true" data-storage-key="b2d42efe-b501-11e8-bb59-047d7badf251"
                                            data-thumbnail="true" data-trash-path="/2175374/projects/15787039/attachments/347427814/trash"
                                            data-trashed="false" data-type="image" data-video-codec="null" data-width="336"
                                            src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/thumbnail.gif"
                                            data-scaled="true" style="width: 336px; height: 280px;"></span>
                                </span>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php get_footer();?>