<?php 
/*
Template Name: brokers
*/
get_header();
get_sidebar();

?>
        <?php $bannerImage = get_field('broker_banner_image');?>

        <section class="inner banner" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">
                <div class="banner_content">

                    <h2><?php the_field('broker_banner_text');?></h2>

                </div>
            </div>
        </section>
        <section class="brokers">
            <div class="container">
                <div class="broker_list">
                    
                    <?php the_field('broker_description');?>

                </div>
                <div class="features_list">
                    <div class="row">
                        <div class="col-sm-6 col-12">
                            <h3><?php the_field('broker_first_feature_title');?></h3>

                            <?php if( have_rows('broker_first_feature') ): ?>

                                <ul>

                                    <?php while( have_rows('broker_first_feature') ): the_row(); 

                                        $firsttitle = get_sub_field('first_title');
                                       
                                    ?>
                                        <li><?php echo $firsttitle; ?></li>
                                    
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                           
                        </div>
                        <div class="col-sm-6 col-12">

                            <h3><?php the_field('broker_second_feature_title');?></h3>
                            
                            <?php if( have_rows('broker_second_repeater') ): ?>

                                <ul>

                                    <?php while( have_rows('broker_second_repeater') ): the_row(); 

                                        $sectitle = get_sub_field('second_title');
                                       
                                    ?>
                                        <li><?php echo $sectitle; ?></li>
                                    
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="button features_btn">
                        
                        <a class="btn-effect" href="<?php the_field('see_more_btn_link');?>"><span><?php the_field('see_more_btn_text');?></span></a>
                    </div>

                    <?php the_field('broker_content');?>

                </div>
            </div>
        </section>

<?php get_footer(); ?>