<?php 

get_header();
get_sidebar();

?>

        <section class="inner banner" style="background-image: url(images/inner-banner.jpg)">
            <div class="container">
                <div class="banner_content">
                    <h2>Membership</h2>
                </div>
            </div>
        </section>
        <section class="membership">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7 col-12 member">
                        <div class="ship_form">
                            <h3>Please login in below to access subscription pricing with available discounts
                                You will receive and email as part of the authentication security process.
                                Two Factor Authentication Process
                            </h3>
                            <form>
                                <div class="row">
                                    <div class="col-sm-6 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="name" placeholder="Contact Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="company" placeholder="Company Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="address" placeholder="Address">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>City</option>
                                                <option>City-1</option>
                                                <option>City-2</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>State</option>
                                                <option>State-1</option>
                                                <option>State-3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>Country</option>
                                                <option>Country</option>
                                                <option>Country</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="mail" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="linkedin" placeholder="Linkedin">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="facebook" placeholder="Facebook">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="website" placeholder="Website">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="video" placeholder="Do you use Video">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="business" placeholder="Type of Business">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-12">
                                        <div class="form-group">
                                            <select class="form-control typical">
                                                <option>Typical Listing Volume</option>
                                                <option>Typical Listing Volume</option>
                                                <option>Typical Listing Volume</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="login" placeholder="Login Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="password" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <figure>
                                            <img src="images/captcha.jpg" alt="Captcha">
                                        </figure>
                                    </div>
                                    <div class="button-submit">
                                        <button class="btn_sbmit" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-5 col-12 member-right">
                        <div class="ship_right">
                            <h4>Download brochure</h4>
                            <figure>
                                <img src="images/member-right.jpg" alt="Member">
                            </figure>
                            <div class="button">
                                <a class="btn-effect" href="#"><span>Download</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php get_footer(); ?>