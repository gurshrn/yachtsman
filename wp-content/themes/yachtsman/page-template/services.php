<?php 
/*
Template Name: services
*/
get_header();
get_sidebar();

?>
        <?php $bannerImage = get_field('service_banner_image');?>

        <section class="inner banner" style="background-image: url(<?php echo $bannerImage['url']?>)">
            <div class="container">
                <div class="banner_content">

                    <h2><?php the_field('service_banner_text'); ?></h2>

                </div>
            </div>
        </section>
        <section class="services">
            <div class="container">

                <h3><?php the_field('service_title'); ?></h3>

                <?php the_field('service_content'); ?>

                <div class="services_area">
                    <div class="row">
                        <div class="col-sm-4 col-12 row_service wow fadeIn" data-wow-delay="0.2s">
                            <div class="service">
                                <figure>
                                    <svg id="legal" x="0px" y="0px" viewBox="0 0 370.402 370.398" enable-background="new 0 0 370.402 370.398"
                                        xml:space="preserve">
                                        <g>
                                            <defs>
                                                <rect id="SVGID_1_" width="370.402" height="370.398" />
                                            </defs>
                                            <clipPath id="SVGID_2_">
                                                <use xlink:href="#SVGID_1_" overflow="visible" />
                                            </clipPath>
                                            <path clip-path="url(#SVGID_2_)" d="M370.391,255.852c0-0.086,0.012-0.168,0.012-0.25c0-0.021-0.004-0.04-0.004-0.06
                                        c0-0.137-0.012-0.27-0.023-0.405l-0.012-0.2c-0.016-0.117-0.035-0.234-0.055-0.351c-0.016-0.087-0.027-0.173-0.047-0.255
                                        c-0.02-0.09-0.043-0.18-0.066-0.27c-0.031-0.109-0.059-0.214-0.094-0.32c-0.02-0.069-0.047-0.137-0.074-0.207
                                        c-0.043-0.121-0.09-0.242-0.141-0.362c-0.012-0.017-0.016-0.035-0.023-0.056L306.402,113.5V95.199h0.398
                                        c6.836,0,12.398-5.562,12.398-12.398v-0.953c0-6.118-4.383-11.266-10.422-12.239L204,52.699V41.957
                                        c3.977-4.457,6.402-10.328,6.402-16.758C210.402,11.305,199.098,0,185.199,0C171.305,0,160,11.305,160,25.199
                                        c0,6.43,2.425,12.301,6.402,16.758v10.16L61.563,69.59c-6.003,1-10.363,6.144-10.363,12.23v0.981
                                        c0,6.836,5.563,12.398,12.403,12.398H64V113.5L0.539,253.117c-0.008,0.021-0.016,0.039-0.023,0.056
                                        c-0.051,0.12-0.098,0.241-0.141,0.362c-0.027,0.067-0.056,0.138-0.078,0.207c-0.035,0.106-0.063,0.211-0.09,0.32
                                        c-0.023,0.09-0.051,0.18-0.07,0.27c-0.016,0.082-0.032,0.168-0.044,0.255c-0.019,0.116-0.043,0.233-0.054,0.351
                                        c-0.009,0.067-0.009,0.133-0.016,0.2c-0.011,0.136-0.019,0.269-0.019,0.405c0,0.02-0.004,0.039-0.004,0.056
                                        c0,0.086,0.007,0.168,0.007,0.254c0.005,0.065,0.005,0.136,0.013,0.203C0.391,280.734,30.987,300,70,300
                                        c39.012,0,69.609-19.266,69.984-43.944c0.003-0.067,0.003-0.138,0.008-0.203c0-0.086,0.008-0.168,0.008-0.25
                                        c0-0.021,0-0.04-0.004-0.06c0-0.133-0.009-0.27-0.019-0.405c-0.004-0.067-0.009-0.133-0.016-0.2
                                        c-0.012-0.117-0.031-0.234-0.056-0.351c-0.01-0.087-0.023-0.169-0.043-0.255c-0.019-0.09-0.046-0.18-0.069-0.27
                                        c-0.027-0.105-0.056-0.214-0.09-0.32c-0.023-0.069-0.051-0.137-0.074-0.207c-0.047-0.121-0.09-0.242-0.145-0.358
                                        c-0.007-0.021-0.011-0.039-0.023-0.06L76,113.5V95.199h90.402v130.356c-3.812,2.117-6.402,6.183-6.402,10.843v88.391
                                        l-38.008,11.879c-7.89,2.466-13.192,9.677-13.192,17.941v9.789c0,3.317,2.687,6,6,6h140.801c3.313,0,6-2.683,6-6v-9.789
                                        c0-8.265-5.301-15.476-13.191-17.945l-38.008-11.875v-88.391c0-4.66-2.591-8.726-6.402-10.843V95.199h90.402V113.5l-63.465,139.617
                                        c-0.008,0.021-0.012,0.039-0.023,0.06c-0.051,0.116-0.094,0.237-0.141,0.358c-0.027,0.07-0.051,0.138-0.074,0.207
                                        c-0.035,0.106-0.063,0.215-0.094,0.32c-0.023,0.09-0.047,0.18-0.066,0.27c-0.02,0.086-0.032,0.168-0.047,0.255
                                        c-0.02,0.116-0.039,0.233-0.055,0.351l-0.012,0.2c-0.012,0.136-0.02,0.272-0.023,0.405c0,0.02-0.004,0.039-0.004,0.06
                                        c0,0.082,0.012,0.164,0.012,0.25c0.004,0.065,0,0.136,0.008,0.203C230.793,280.734,261.391,300,300.402,300
                                        s69.604-19.266,69.979-43.944C370.391,255.988,370.387,255.917,370.391,255.852 M70,129.301l54.684,120.301H15.319L70,129.301z
                                        M70,288c-27.782,0-51.754-11.586-56.953-26.397h113.91C121.754,276.414,97.78,288,70,288 M198.402,236.398v86.801H172v-86.801
                                        c0-0.218,0.18-0.398,0.402-0.398H198C198.223,236,198.402,236.18,198.402,236.398 M178.402,224V49.461
                                        c2.164,0.605,4.441,0.937,6.797,0.937c2.359,0,4.637-0.332,6.801-0.937V224H178.402z M185.199,12
                                        c7.281,0,13.203,5.922,13.203,13.199c0,7.278-5.922,13.199-13.203,13.199c-7.277,0-13.199-5.921-13.199-13.199
                                        C172,17.922,177.922,12,185.199,12 M63.603,83.199c-0.223,0-0.403-0.179-0.403-0.398V81.82c0-0.195,0.141-0.359,0.336-0.394
                                        l102.867-17.145v18.918H63.603z M249.602,354.609v3.789H120.8v-3.789c0-2.988,1.918-5.597,4.774-6.488l41.344-12.922h36.566
                                        l41.344,12.922C247.684,349.012,249.602,351.621,249.602,354.609 M204,64.855l102.863,16.602c0.195,0.031,0.336,0.195,0.336,0.395
                                        v0.949c0,0.222-0.18,0.398-0.398,0.398H204V64.855z M300.402,129.301l54.68,120.301H245.719L300.402,129.301z M300.402,288
                                        c-27.785,0-51.754-11.586-56.957-26.397h113.91C352.156,276.414,328.184,288,300.402,288" />
                                        </g>
                                    </svg>
                                </figure>
                                <div class="service_content">
                                    <h4>Legal</h4>
                                    <p>
                                        Maritime law is one of the oldest disciplines in the legal profession. Simple
                                        processes and procedures need to be
                                        reviewed by a professional, sometimes before entering port or making an
                                        acquisition. Consult with a professional is a
                                        good idea.
                                    </p>
                                    <a href="#">Click Here</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-12 row_service wow fadeIn" data-wow-delay="0.4s">
                            <div class="service">
                                <figure>
                                    <svg id="banking" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                                        x="0px" y="0px" viewBox="-33 149 480 488" style="enable-background:new -33 149 480 488;"
                                        xml:space="preserve">
                                        <path d="M179.3,180.1c31.6-19.8,71.7-19.8,103.3,0l8.6-13.5c-36.8-23.5-83.9-23.5-120.6,0L179.3,180.1z" />
                                        <path d="M119,261c0,14.8,2.9,29.5,8.6,43.1l14.8-6.2c-4.9-11.7-7.4-24.3-7.4-36.9H119z" />
                                        <path d="M231,373c14.8,0,29.4-2.9,43.1-8.6l-6.2-14.8c-11.7,4.9-24.2,7.4-36.9,7.4V373z" />
                                        <path d="M334.4,304.1c5.7-13.6,8.6-28.3,8.6-43.1h-16c0,12.7-2.5,25.2-7.4,36.9L334.4,304.1z" />
                                        <path d="M311,261c0-44.2-35.8-80-80-80s-80,35.8-80,80s35.8,80,80,80C275.2,341,311,305.2,311,261z M231,325c-35.3,0-64-28.7-64-64
                                        s28.7-64,64-64s64,28.7,64,64C295,296.3,266.3,325,231,325z" />
                                        <path d="M247,245h16c-0.7-13.5-10.7-24.7-24-27v-5h-16v5c-13.3,2.3-23.3,13.5-24,27c0.3,8.6,6.1,16.1,14.4,18.6l9.6,3.2v20.5
                                        c-4.5-1.6-7.6-5.6-8-10.3h-16c0.7,13.5,10.7,24.7,24,27v5h16v-5c13.3-2.3,23.3-13.5,24-27c-0.3-8.6-6.1-16.1-14.4-18.6l-9.6-3.2
                                        v-20.5C243.5,236.3,246.6,240.3,247,245z M243.6,273.6c2,0.7,3.4,2.1,3.4,3.4c-0.4,4.7-3.5,8.7-8,10.3v-15.2L243.6,273.6z
                                        M218.4,248.4c-2-0.7-3.4-2.1-3.4-3.4c0.4-4.7,3.5-8.7,8-10.3v15.2L218.4,248.4z" />
                                        <path d="M340.7,311.3c-3.1-3.1-8.2-3.1-11.3,0L227,413.7L116.7,303.3c-3.1-3.1-8.2-3.1-11.3,0l-104,104c-3.1,3.1-3.1,8.2,0,11.3
                                        L59.7,477H-33v16H71c1.1,0,2.2-0.2,3.2-0.6l54.5-23.4H247v32h-88c-4.4,0-8,3.6-8,8c0,3.1,1.8,6,4.6,7.3l104,48
                                        c2.6,1.2,5.7,0.9,8-0.8l138.6-100l21.1,21.1L268,621h-43.6L73.8,565.5c-0.9-0.3-1.8-0.5-2.8-0.5H-33v16H69.6l150.7,55.5
                                        c0.9,0.3,1.8,0.5,2.8,0.5h48c1.9,0,3.8-0.7,5.2-1.9l168-144c3.4-2.9,3.7-7.9,0.9-11.3c-0.1-0.2-0.3-0.3-0.4-0.4L418.3,453l26.3-26.3
                                        c3.1-3.1,3.1-8.2,0-11.3L340.7,311.3z M111,320.3L243.7,453h-31c-7-23.8-28.9-40.1-53.6-40c-28.3,0.2-52.1,21.3-55.6,49.4l-25,10.7
                                        L18.3,413L111,320.3z M305.6,492.2l-41.8-41.8c3-17.1,17.9-29.5,35.2-29.4c19.8,0,35.9,16,36,35.9
                                        C335,474.2,322.6,489.1,305.6,492.2z M327.9,500.2l-8.3,6l-1-1C321.8,503.9,324.9,502.2,327.9,500.2L327.9,500.2z M127,453
                                        c-1.1,0-2.2,0.2-3.2,0.6l-1.9,0.8c7.9-20.5,30.9-30.7,51.4-22.8c10,3.8,18,11.5,22.2,21.4H127z M262.1,547.8L195.4,517H255
                                        c4.4,0,8-3.6,8-8v-36.7l43.4,43.4L262.1,547.8z M401.8,446.9l-70.5,50.9c22.5-17.9,26.2-50.6,8.3-73.1
                                        c-17.9-22.5-50.6-26.2-73.1-8.3c-7,5.5-12.4,12.8-15.7,21L238.3,425l96.7-96.7l92.7,92.7L401.8,446.9z" />
                                    </svg>
                                </figure>
                                <div class="service_content">
                                    <h4>Banking</h4>
                                    <p>
                                        Worldwide Yachtsman are always identifying lenders who understand the needs of
                                        buyers and also sellers. Start with
                                        these vendors who are familiar with the boat buying and selling process and
                                        offer VERY competitive rates over short and
                                        long terms.
                                    </p>
                                    <a href="#">Click Here</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-12 row_service wow fadeIn" data-wow-delay="0.6s">
                            <div class="service">
                                <figure>
                                    <svg id="insurance" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                                        x="0px" y="0px" width="503.5px" height="512px" viewBox="0 0 503.5 512" style="enable-background:new 0 0 503.5 512;"
                                        xml:space="preserve">
                                        <path d="M418.1,0H76.8c-5.1,0-8.5,3.4-8.5,8.5v34.1H34.1C15.4,42.7,0,58,0,76.8v102.4c0,18.8,15.4,34.1,34.1,34.1h34.1v290.1
                                            c0,5.1,3.4,8.5,8.5,8.5h341.3c5.1,0,8.5-3.4,8.5-8.5V290.1h-17.1v204.8H85.3V213.3h136.5c18.8,0,34.1-15.4,34.1-34.1V76.8
                                            c0-18.8-15.4-34.1-34.1-34.1H85.3V17.1h324.3v128h17.1V8.5C426.7,3.4,423.3,0,418.1,0L418.1,0z M238.9,179.2
                                            c0,9.4-7.7,17.1-17.1,17.1H34.1c-9.4,0-17.1-7.7-17.1-17.1V128h221.9V179.2z M238.9,110.9H17.1V93.9h221.9V110.9z M221.9,59.7
                                            c9.4,0,17.1,7.7,17.1,17.1H17.1c0-9.4,7.7-17.1,17.1-17.1H221.9z M221.9,59.7" />
                                        <path d="M424.1,253.4c3.4-3.4,3.4-8.5,0-11.9L390,207.4c-3.4-3.4-8.5-3.4-11.9,0L241.5,343.9c-0.9,0.9-1.7,1.7-1.7,2.6l-25.6,59.7
	                                        c-1.7,4.3,0,9.4,4.3,11.1c0.9,0.9,2.6,0.9,3.4,0.9c0.9,0,2.6,0,3.4-0.9l59.7-25.6c0.9,0,1.7-0.9,2.6-1.7L424.1,253.4z M238.1,393.4
                                            l11.9-28.2l16.2,16.2L238.1,393.4z M281.6,372.1l-22.2-22.2L384,225.3l22.2,22.2L281.6,372.1z M281.6,372.1" />
                                        <path d="M458.2,224.4L390,156.2c-3.4-3.4-8.5-3.4-11.9,0L301.2,233l11.9,11.9l70.8-70.8l62.3,62.3L458.2,224.4z M458.2,224.4" />
                                        <path d="M475.3,130.6c-3.4-3.4-8.5-3.4-11.9,0l-25.6,25.6c-3.4,3.4-3.4,8.5,0,11.9l25.6,25.6c3.4,3.4,8.5,3.4,11.9,0l25.6-25.6
                                        c3.4-3.4,3.4-8.5,0-11.9L475.3,130.6z M469.3,175.8l-13.7-13.7l13.7-13.7l13.7,13.7L469.3,175.8z M469.3,175.8" />
                                        <path d="M146.8,454c3.4-3.4,9.4-3.4,13.7,0c10.2,10.2,27.3,10.2,37.5,0l12.8-12.8l-11.9-11.9L186,442c-3.4,3.4-9.4,3.4-13.7,0
                                        c-10.2-10.2-27.3-10.2-37.5,0L122,454.8l11.9,11.9L146.8,454z M146.8,454" />
                                        <rect x="102.4" y="230.4" width="162.1" height="17.1" />
                                        <rect x="102.4" y="281.6" width="128" height="17.1" />
                                        <rect x="102.4" y="332.8" width="102.4" height="17.1" />
                                        <path d="M358.4,119.5c5.1,0,8.5-3.4,8.5-8.5V59.7c0-5.1-3.4-8.5-8.5-8.5h-68.3c-5.1,0-8.5,3.4-8.5,8.5v51.2c0,5.1,3.4,8.5,8.5,8.5
	                                    H358.4z M298.7,68.3h51.2v34.1h-51.2V68.3z M298.7,68.3" />
                                    </svg>
                                </figure>
                                <div class="service_content">
                                    <h4>Insurance</h4>
                                    <p>
                                        Be sure to cover the boat of your dreams with insurance. Please contact any of
                                        these companies to get competive prices
                                        on robust liability coverage to protect your investment.
                                    </p>
                                    <a href="#">Click Here</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-12 row_service wow fadeIn" data-wow-delay="0.6s">
                            <div class="service">
                                <figure>
                                    <svg id="transport" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                                        x="0px" y="0px" viewBox="26.2 284.2 362 225.2" style="enable-background:new 26.2 284.2 362 225.2;"
                                        xml:space="preserve">
                                        <path d="M381.4,391h-74.8l-50.4-58c-1.2-1.2-2.8-2-4.4-2h-60c0-0.4,0-0.8-0.4-1.2L169,287.4c-1.2-2-3.2-3.2-5.2-3.2h-30
                                        c-3.2,0-6,2.8-6,6v40.4h-24c-2,0-4,1.2-5.2,2.8l-34,57.2H47c-3.2,0-6,2.4-6,6c0,1.2-0.4,24.4,9.2,45.2c-4.8-2-10.8-3.2-18-3.2
                                        c-3.2,0-6,2.8-6,6s2.8,6,6,6c13.2,0,19.6,4.8,27.2,10.8c8,6,16.8,13.2,34.4,13.2c0.8,0,1.6,0,2.4,0c1.2,0,2,0,3.2,0
                                        c0.8,0,1.6-0.4,2.4-0.8c12.4-1.6,19.6-7.6,26.4-12.4c7.2-5.6,13.6-10.8,27.2-10.8c13.2,0,19.6,4.8,27.2,10.8
                                        c8,6,16.8,13.2,34.4,13.2s26.4-7.2,34.4-13.2c7.2-5.6,13.6-10.8,27.2-10.8c13.2,0,19.6,4.8,27.2,10.8c8,6,16.8,13.2,34.4,13.2
                                        c3.2,0,6-2.8,6-6s-2.8-6-6-6c-4.4,0-8-0.4-11.2-1.6l57.6-60c1.6-1.6,2-4.4,1.2-6.4C386.2,392.6,383.8,391,381.4,391z M159.8,296.6
                                        l2.4,4.4h-22.8v-4.4H159.8z M139.8,313h28.8l9.6,18.4h-38.4L139.8,313L139.8,313z M107.4,343H249l18.8,21.6H94.2L107.4,343z
                                        M87.4,376.6h190.8L291,391H78.6L87.4,376.6z M317,455.8c-1.6-1.2-3.2-2.4-4.8-3.6c-8-6-16.8-13.2-34.4-13.2s-26.4,7.2-34.4,13.2
                                        c-7.2,5.6-13.6,10.8-27.2,10.8c-13.2,0-19.6-4.8-27.2-10.8c-8-6-16.8-13.2-34.4-13.2c-17.2,0-26.4,7.2-34.4,13.2
                                        c-6.8,5.2-12.8,10-24.4,10.4c-10.4-0.4-18.8-4.4-25.6-11.2c-13.6-14-16.8-37.6-17.6-48.4H367L317,455.8z" />
                                        <path d="M93.8,509.4c17.2,0,26.4-7.2,34.4-13.2c7.2-5.6,13.6-10.8,27.2-10.8c13.2,0,19.6,4.8,27.2,10.8c8,6,16.8,13.2,34.4,13.2
                                        s26.4-7.2,34.4-13.2c7.2-5.6,13.6-10.8,27.2-10.8c13.2,0,19.6,4.8,27.2,10.8c8,6,16.8,13.2,34.4,13.2c3.2,0,6-2.8,6-6s-2.8-6-6-6
                                        c-13.2,0-19.6-4.8-27.2-10.8c-8-6-16.8-13.2-34.4-13.2s-26.4,7.2-34.4,13.2c-7.2,5.6-13.6,10.8-27.2,10.8
                                        c-13.2,0-19.6-4.8-27.2-10.8c-8-6-16.8-13.2-34.4-13.2c-17.2,0-26.4,7.2-34.4,13.2c-7.2,5.6-13.6,10.8-27.2,10.8
                                        c-13.2,0-19.6-4.8-27.2-10.8c-8-6-16.8-13.2-34.4-13.2c-3.2,0-6,2.8-6,6s2.8,6,6,6c13.2,0,19.6,4.8,27.2,10.8
                                        C67.4,502.6,76.6,509.4,93.8,509.4z" />
                                    </svg>

                                </figure>
                                <div class="service_content">
                                    <h4>Transport</h4>
                                    <p>
                                        Take the work out of moving a boat or yacht either by a) trailer, b) ship or c)
                                        via a captain on the worlds oceans and
                                        water ways. Contact any of these marine logistic professionals for good advice
                                        and pricing to make your boat arrive
                                        safely.
                                    </p>
                                    <a href="#">Click Here</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-12 row_service wow fadeIn" data-wow-delay="0.4s">
                            <div class="service">
                                <figure>
                                    <svg version="1.1" id="surveyor" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;"
                                        xmlns:graph="&ns_graphs;" x="0px" y="0px" viewBox="-49 162.3 512 469.3" style="enable-background:new -49 162.3 512 469.3;"
                                        xml:space="preserve">
                                        <g>
                                            <circle cx="4.3" cy="215.7" r="10.7" />
                                            <circle cx="47" cy="215.7" r="10.7" />
                                            <circle cx="89.7" cy="215.7" r="10.7" />
                                            <path d="M68.3,375.7H303c5.9,0,10.7-4.8,10.7-10.7s-4.8-10.7-10.7-10.7H68.3c-5.9,0-10.7,4.8-10.7,10.7S62.4,375.7,68.3,375.7z" />
                                            <path d="M196.3,482.3h-128c-5.9,0-10.7,4.8-10.7,10.7s4.8,10.7,10.7,10.7h128c5.9,0,10.7-4.8,10.7-10.7S202.2,482.3,196.3,482.3z" />
                                            <path d="M422.2,290.3c-0.6,0-1.2,0.2-1.8,0.2V205c0-23.5-19.1-42.7-42.7-42.7h-384c-23.5,0-42.7,19.1-42.7,42.7v384
                                            c0,23.5,19.1,42.7,42.7,42.7h384c23.5,0,42.7-19.1,42.7-42.7V390.7L451,360c0,0,0,0,0,0c7.6-7.6,12-18.1,12-28.9
                                            C463,308.6,444.7,290.3,422.2,290.3z M-6.3,183.7h384c11.8,0,21.3,9.6,21.3,21.3v42.7H-27.7V205C-27.7,193.2-18.1,183.7-6.3,183.7z
                                            M399,589c0,11.8-9.6,21.3-21.3,21.3h-384c-11.8,0-21.3-9.6-21.3-21.3V269H399v28.6c-2,1.4-4,2.9-5.7,4.7l-116.1,116H68.3
                                            c-5.9,0-10.7,4.8-10.7,10.7s4.8,10.7,10.7,10.7h191.8l-10.2,51.2c-0.7,3.5,0.4,7.1,2.9,9.6c2,2,4.8,3.1,7.5,3.1
                                            c0.7,0,1.4-0.1,2.1-0.2l53.3-10.7c2.1-0.4,4-1.4,5.5-2.9l77.8-77.8V589z M308.4,472.5l-34.5,6.9l6.9-34.5l96.8-96.8l27.6,27.6
                                            L308.4,472.5z M436,345l-15.6,15.6L392.7,333l15.6-15.6c3.6-3.6,8.6-5.7,13.8-5.7c10.8,0,19.5,8.8,19.5,19.5
                                            C441.7,336.3,439.6,341.3,436,345z" />
                                        </g>
                                    </svg>
                                </figure>
                                <div class="service_content">
                                    <h4>Surveyor</h4>
                                    <p>
                                        Sometimes over looked, but it’s always advised to hire a marine surveyor
                                        professional to inspect and write up a full
                                        report for your records, insurance company, resell value and general knowledge.
                                        Let them explain to you the process.
                                    </p>
                                    <a href="#">Click Here</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-12 row_service wow fadeIn" data-wow-delay="0.2s">
                            <div class="service">
                                <figure>
                                    <svg id="registration" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                                        x="0px" y="0px" viewBox="-33 157 480 480" style="enable-background:new -33 157 480 480;"
                                        xml:space="preserve">
                                        <path d="M407,253h-88v-96h-16v184c0,4.4,3.6,8,8,8h27.1L207,427.7L75.9,349H103c4.4,0,8-3.6,8-8V157H95v96H7c-22.1,0-40,17.9-40,40
                                        v224c0,22.1,17.9,40,40,40h131.1l-16,32H-25c-4.4,0-8,3.6-8,8c0,40,42.2,40,56,40h368c13.8,0,56,0,56-40c0-4.4-3.6-8-8-8H291.9
                                        l-16-32H407c22.1,0,40-17.9,40-40V293C447,270.9,429.1,253,407,253z M47,333c-4.4,0-8,3.6-8,8c0,2.8,1.5,5.4,3.9,6.9l160,96
                                        c2.5,1.5,5.7,1.5,8.2,0l160-96c3.8-2.3,5-7.2,2.7-11c-1.4-2.4-4-3.9-6.9-3.9h-48v-32h80v192H15V301h80v32H47z M391,621H23
                                        c-26.6,0-36.2-6.2-39-16H430C427.2,614.8,417.6,621,391,621z M274.1,589H139.9l16-32h102.1L274.1,589z M431,517
                                        c0,13.3-10.7,24-24,24H7c-13.3,0-24-10.7-24-24V293c0-13.3,10.7-24,24-24h88v16H7c-4.4,0-8,3.6-8,8v208c0,4.4,3.6,8,8,8h400
                                        c4.4,0,8-3.6,8-8V293c0-4.4-3.6-8-8-8h-88v-16h88c13.3,0,24,10.7,24,24V517z" />
                                        <path d="M135,319.4V349h16v-29.6c0-14.6,11.8-26.4,26.4-26.4h59.2c14.6,0,26.4,11.8,26.4,26.4V349h16v-29.6
                                        c0-21.4-15.9-39.4-37.1-42c12.5-9.8,20.2-24.5,21.1-40.4c0-52.9-30.5-64-56-64s-56,11.1-56,64c0.9,15.9,8.6,30.6,21.1,40.4
                                        C150.9,280,135,298,135,319.4z M168.1,244.6c13.7-1.1,38.4-5.3,49.9-20.4l28.6,17.2c-3.2,19.6-27.3,35.7-39.6,35.7
                                        C189.2,274,174.3,261.6,168.1,244.6L168.1,244.6z M207,189c22.7,0,35.2,10.7,38.8,33.1l-26.7-16c-3.8-2.3-8.7-1-11,2.7
                                        c-0.3,0.5-0.5,1-0.7,1.6c-4,12.1-25.9,16.7-40,18.1C169.6,201.7,182.2,189,207,189z" />
                                        <polygon points="351,477 367,477 367,461 383,461 383,445 367,445 367,429 351,429 351,445 335,445 335,461 351,461 " />
                                    </svg>
                                </figure>
                                <div class="service_content">
                                    <h4>Registration</h4>
                                    <p>
                                        It’s a good idea to talk with registration specialist prior to purchase to
                                        fully understand the liabilities both
                                        financially and otherwise prior to a purchase. They folks understand the
                                        process.
                                    </p>
                                    <a href="#">Click Here</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php the_field('service_description'); ?>

                </div>
            </div>
        </section>

<?php get_footer();?>