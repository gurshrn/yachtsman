<?php 
/*
Template Name: login-page
*/
get_header();   


if(isset($_POST['register']) && $_POST['register'] == 'register')
{
	global $wpdb, $user_ID; 
	if ($user_ID) 
	{  
	   header( 'Location:' . get_site_url() );  
	   
	} 
	else
	{
		$errors = array();  
   
    	if( $_SERVER['REQUEST_METHOD'] == 'POST' ) 
      	{  
   
	       /* $email = $wpdb->escape($_REQUEST['email']);  
	        if( !is_email( $email ) ) 
	        {   
	            $errors['email'] = "Please enter a valid email";  
	        } elseif( email_exists( $email ) ) 
	        {  
	            $errors['email'] = "This email address is already in use";  
	        }  */
	   
	        // Check password is valid  
	        
	   
	        if(0 === count($errors)) 
	         {  
	   
	            $password = $_POST['password'];  
	            $email = $_POST['email'];  
	   
	            $new_user_id = wp_create_user( $email, $password, $email );  
	   
	            // You could do all manner of other things here like send an email to the user, etc. I leave that to you.  
	   
	            header( 'Location:' . get_site_url() ); 
	   
	            //header( 'Location:' . get_bloginfo('url') . '/login/?success=1&u=' . $username );  
	   
	        }  
	   
	    }  
	}
}

if(isset($_POST['login']) && $_POST['login'] == 'login')
{
	global $wpdb;
	$email = $wpdb->escape($_REQUEST['email']);  
    $password = $wpdb->escape($_REQUEST['password']);  
    //$remember = $wpdb->escape($_REQUEST['remember']);  
   
    //if($remember) $remember = "true";  
    //else $remember = "false";  
   
    $login_data = array();  
    $login_data['email'] = $email;  
    $login_data['password'] = $password;  
    //$login_data['remember'] = $remember;  
   
    $user_verify = wp_signon( $login_data, true );
       
    if ( is_wp_error($user_verify) )   
    {  
        echo "Invalid login details";  
       // Note, I have created a page called "Error" that is a child of the login page to handle errors. This can be anything, but it seemed a good way to me to handle errors.  
     } else
    {    
       echo "<script type='text/javascript'>window.location.href='". get_site_url() ."'</script>";  
       exit();  
     }  
}
  
?>