<?php 
/*
Template Name: advance-search
*/
get_header();
get_sidebar();

?>

        <section class="inner banner" style="background-image: url(images/inner-banner.jpg)">
            <div class="container">
                <div class="banner_content">
                    <h2>Advanced Boat Search</h2>
                    <!-- <h2>Worldwide Yacht Job Postings</h2> -->
                </div>
            </div>
        </section>
        <section class="job_posting">
            <div class="container">
                <div class="posting">
                    <form id="advance-search-form" action="<?php echo get_site_url().'/products/';?>" method="post" autocomplete="off">
                        <div class="row">
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="keyword" placeholder="Keyword">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="new">
                                        <label>New/Used</label>
                                        <input type="radio" name="condition" value="all" checked>All</button>
                                        <input type="radio" name="condition" value="new">New</button>
                                        <input type="radio" name="condition" value="used">Used</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="manufacturermodel" placeholder="Manufacturer/Model">
                                </div>
                            </div>
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Hull Material</option>
                                        <option>Hull Material</option>
                                        <option>Hull Material</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Fuel</option>
                                        <option>Fuel</option>
                                        <option>Fuel</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Number of Engines</option>
                                        <option>Number of Engines</option>
                                        <option>Number of Engines</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <select class="form-control typical">
                                        <option>Boat Type</option>
                                        <option>All Power & Sail</option>
                                        <option>-----</option>
                                        <option>All Power</option>
                                        <option>+Aft Cabin</option>
                                        <option>+Antique and Classic</option>
                                        <option>+Barge</option>
                                        <option>+Bowrider</option>
                                        <option>+Cargo Ship</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="year">
                                        <div class="lenght">
                                            <label>Length</label>
                                            
                                            <input class="form-control" type="text" name="to">
                                            
                                            <small>To</small>
                                            
                                            <input class="form-control" type="text" name="from">
                                        
                                        </div>
                                        <div class="lenght">
                                            <label>Year</label>
                                            <input class="form-control" type="text" name="blank">
                                            <small>To</small>
                                            <input class="form-control" type="text" name="blank">
                                        </div>
                                        <div class="lenght">
                                            <label>Price</label>
                                            <input class="form-control" type="text" name="blank">
                                            <small>To</small>
                                            <input class="form-control" type="text" name="blank">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="unity">
                                        <strong>Unit</strong>
                                        
                                        <input  type="radio" name="length_type">Feet</button>
                                        <input type="radio" name="length_type" checked>Meters</button>
                                        
                                        <diV class="unity_select">
                                            <select class="form-control">
                                                <option>Currency</option>
                                                <option>Currency</option>
                                                <option>Currency</option>
                                            </select>
                                        </diV>
                                        <diV class="unity_select">
                                            <select class="form-control">
                                                <option>City</option>
                                                <option>City</option>
                                                <option>City</option>
                                            </select>
                                        </diV>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>State/Province</option>
                                        <option>All</option>
                                        <option>Alabama</option>
                                        <option>Alaska</option>
                                        <option>Arizona</option>
                                        <option>Arkansa</option>
                                        <option>California</option>
                                        <option>Colorado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Worldwide Regions</option>
                                        <option>All</option>
                                        <option>Northeast (CT, MA, ME, NB, NH, NL, NS, NY, PE,</option>
                                        <option>Mid-Atlantic (DC, DE, MD, NJ, PA, VA, WV)</option>
                                        <option>Great Lakes (IL, IN, MI, MN, OH, ON, PA, QC,</option>
                                        <option>MIdwest (IA, KS, MO, NE, ND, SD)</option>
                                        <option>Southeast (FL, GA, KY, NC, PR, SC, TN, VI)</option>
                                        <option>Gulf Coast (AL, FL, LA, MS, TX)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>All</option>
                                        <option>United State</option>
                                        <option>Canada</option>
                                        <option>Afghanistan</option>
                                        <option>Aland Island</option>
                                        <option>Albania</option>
                                        <option>Algeria</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <select class="form-control typical">
                                        <option>Boats Added Recently</option>
                                        <option>Boats Added Recently</option>
                                        <option>Boats Added Recently</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="check_area">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" checked><span class="new-checkbox"></span>Exclude
                                            Fractional
                                            Boats</label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"><span class="new-checkbox"></span> Boats with videos</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form_submit">
                                    <button class="blue" type="submit">Search</button>
                                    <button class="dark-blue reset-form" type="button">Clear the From</button>
                                    <button class="green" type="button">Save Search</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </section>

<?php get_footer();?>

<script>

    jQuery(document).on('click','.reset-form',function(){
        $('#advance-search-form')[0].reset();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

</script>