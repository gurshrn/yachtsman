<?php 
/*
Template Name: features
*/
get_header();
get_sidebar();

?>
    <?php $bannerImage = get_field('feature_banner_image');?>

        <section class="inner banner" style="background-image: url(<?php echo $bannerImage['url'];?>)">
            <div class="container">
                <div class="banner_content">
                    <h2><?php the_field('feature_banner_text');?></h2>
                </div>
            </div>
        </section>
        <section class="features">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 col-12">
                        <div class="features_left">

                            <?php
                                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                $args = array(
                                  'post_type'   => 'boat-feature',
                                  'post_status' => 'publish',
                                  'posts_per_page' => 3,
                                  'paged' => $paged,
                                  'order' => 'DESC',
                                );
                                $products = new WP_Query( $args );
                                
                                if( $products->have_posts() ) :
                                    
                                    while( $products->have_posts() ) :
                                        
                                        $products->the_post();
                            ?>

                                <div class="feature_head">
                                    <div class="feature_img">
                                        <figure style="background-image: url(<?php echo the_post_thumbnail_url();?>)"></figure>
                                    </div>
                                    <div class="feature_content">
                                        <h4><?php echo  get_the_title(); ?></h4>
                                        <span><?php echo get_the_date('M d, Y'); ?></span>
                                        <p>
                                           <?php the_excerpt(); ?>
                                        </p>
                                        <div class="features_social">
                                            <a href="<?php the_permalink();?>">Read More</a>
                                            <ul>
                                                <li> <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            
                            <?php 

                                endwhile;
                                wp_reset_postdata();
                                pagination($products->max_num_pages);
                                endif;
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-12">
                        <div class="blog_right">
                            <form>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="search" placeholder="Search...">
                                    <i class="fa fa-search"></i>
                                </div>
                            </form>
                            <div class="recent_post">
                                <h4>Recent Articles</h4>

                                <?php
                                    $args = array(
                                      'post_type'   => 'boat-feature',
                                      'post_status' => 'publish',
                                      'posts_per_page' => 5,
                                      'order' => 'DESC',
                                    );
                                    $products = new WP_Query( $args );
                                    
                                    if( $products->have_posts() ) :
                                        
                                        while( $products->have_posts() ) :
                                            
                                            $products->the_post();

                                ?>

                                        <div class="post">
                                            <figure>
                                                <img src="<?php echo the_post_thumbnail_url();?>" alt="Post">
                                            </figure>
                                            <div class="post_head">
                                                <a href="<?php the_permalink();?>"><?php echo  get_the_title(); ?></a>
                                                <p>
                                                    <?php 
                                                        
                                                        $trimexcerpt = get_the_excerpt();
                                                        
                                                        $shortexcerpt = wp_trim_words( $trimexcerpt, $num_words = 10, $more = '… ' );
                                                        
                                                        echo $shortexcerpt;

                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                <?php 

                                    endwhile;
                                    wp_reset_postdata();
                                    endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php get_footer();?>