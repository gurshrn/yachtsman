<?php 
/*
Template Name: home
*/
get_header();
get_sidebar();

global $post;



?>
        <?php $bannerImage = get_field('banner_image');?>

        <section class="banner" style="background-image: url(<?php echo $bannerImage['url'];?>)">
            <div class="container">
                <div class="banner_content">
                    
                    <h2><?php the_field('banner_title');?></h2>

                    <?php the_field('banner_description');?>

                </div>
            </div>
        </section>
        
        <?php get_sidebar('form');?>

        <div class="ads_area">
            <div class="container">
                <figure>
                    <img src="https://via.placeholder.com/970x90" alt="Ads">
                </figure>
            </div>
        </div>

        <section class="latest-listing">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 slide-full">
                        <h2>Latest <strong>listings</strong></h2>
                        <div id="listingSlider" class="owl-carousel">

                            <?php
                                $args = array(
                                  'post_type'   => 'boat',
                                  'post_status' => 'publish',
                                  
                                );
                                $boats = new WP_Query( $args );
                                
                                if( $boats->have_posts() ) :
                                    
                                    while( $boats->have_posts() ) :
                                        
                                        $boats->the_post();

                            ?>
                                    <div class="item">
                                        <div class="listing">
                                            
                                            <figure style="background-image: url(<?php echo the_post_thumbnail_url();?>)"></figure>
                                            
                                            <div class="listing_content">
                                                
                                                <h4><?php the_title();?></h4>
                                                
                                                <strong><?php the_field('currency','options').' '.the_field('price');?>**</strong>
                                                
                                                <p>
                                                    <?php
                                                        $string = get_field('countries');
                                                        echo ucwords(str_replace("_"," ",$string));
                                                    ?>
                                                </p>
                                                
                                                <div class="button">
                                                    <a class="btn-effect" href="<?php the_permalink();?>"><span>View Details</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php 

                                endwhile;
                                wp_reset_postdata();
                                endif;
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-2 slide_Ads">
                        <div class="right_ads">
                            <img src="https://via.placeholder.com/160x600" alt="Ads">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="featured-boats">
            <div class="container">
                
                <h2>Featured <strong>Boats</strong></h2>
                
                <div class="boats_listing">
                    <div class="row">

                        <?php
                            $args = array(
                              'post_type'   => 'boat',
                              'post_status' => 'publish',
                              
                            );
                            $featureBoats = new WP_Query( $args );
                            
                            if( $featureBoats->have_posts() ) :
                                
                                while( $featureBoats->have_posts() ) :

                                    $featureBoats->the_post();

                                    $featured = get_field('featured');
                                    
                                    if($featured[0] == 'yes'):
                        ?>
                                        <div class="col-md-4 col-sm-4 col-12 wow fadeIn" data-wow-delay="0.2s" data-wow-duration="1000ms"
                                            style="visibility: visible; animation-duration: 1000ms; animation-delay: 0.2s; animation-name: fadeIn;">
                                            <div class="listing">
                                                
                                                <figure style="background-image: url(<?php echo the_post_thumbnail_url();?>)"></figure>

                                                <div class="listing_content">

                                                    <h4><?php the_title();?></h4>

                                                    <strong><?php the_field('currency','options').' '.the_field('price');?>**</strong>
                                                    <p>
                                                        <?php
                                                            $string = get_field('countries');
                                                            echo ucwords(str_replace("_"," ",$string));
                                                        ?>
                                                    </p>
                                                    
                                                    <div class="button">
                                                        <a class="btn-effect" href="<?php the_permalink();?>"><span>View Details</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                            <?php 
                                endif;
                                endwhile;
                                wp_reset_postdata();
                                endif;
                            ?>
                        
                        
                        <div class="button boats">
                            <a class="btn-effect" href="<?php echo get_site_url().'/product';?>"><span>View All</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Out-BOard-Only -->
        <section class="out_board">
            <div class="container">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#antiques">Antiques and Classics</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#outboards">Outboards Only</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#center">Center Consoles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#freshwater">Freshwater Boats</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#bank">Bank Owned</a>
                    </li>
                </ul>
                <div class="tab-content boats">
                    <div id="antiques" class="tab-pane">
                        <div class="row equl">
                            <div class="col-sm-12">
                                <strong style="text-align:center; font-size:28px; color:#fff;">Antiques and
                                    Classics</strong>
                            </div>
                        </div>
                    </div>
                    <div id="outboards" class="tab-pane active">
                        <div class="row equl">
                            <div class="col-sm-6 col-12 boats_slide">
                                <div class="property_slide">
                                    <div class="slider">
                                        <div id="slider" class="flexslider">
                                            <ul class="slides">
                                                <li>
                                                    <figure style="background-image:url(images/product_11.jpg)"></figure>
                                                </li>
                                                <li>
                                                    <figure style="background-image:url(images/product_1.jpg)"></figure>
                                                </li>
                                                <li>
                                                    <figure style="background-image:url(images/product_2.jpg)"></figure>
                                                </li>
                                                <li>
                                                    <figure style="background-image:url(images/product_1.jpg)"></figure>
                                                </li>
                                                <li>
                                                    <figure style="background-image:url(images/product_2.jpg)"></figure>
                                                </li>

                                            </ul>
                                        </div>
                                        <!-- <div id="carousel" class="flexslider">
                                                <ul class="slides">
                                                    <li>
                                                        <figure style="background-image:url(images/thumb_1.jpg)"></figure>
                                                    </li>
                                                    <li>
                                                        <figure style="background-image:url(images/thumb_2.jpg)"></figure>
                                                    </li>
                                                    <li>
                                                        <figure style="background-image:url(images/thumb_3.jpg)"></figure>
                                                    </li>
                                                    <li>
                                                        <figure style="background-image:url(images/thumb_4.jpg)"></figure>
                                                    </li>
                                                </ul>
                                            </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-12 boats_content">
                                <div class="out_content">
                                    <h3>outboards <strong>ONLY</strong></h3>
                                    <p>
                                        Looking to save time? Click here for a direct link to all outboard powered
                                        boats. This is the industry's first
                                        one-click shopping for outboard powered vessels!
                                    </p>
                                    <div class="button boats out">
                                        <a class="btn-effect" href="#"><span>View Boats</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="center" class="tab-pane">
                        <div class="row equl">
                            <div class="col-sm-12">
                                <strong style="text-align:center; font-size:28px; color:#fff;">Center Consoles</strong>
                            </div>
                        </div>
                    </div>
                    <div id="freshwater" class="tab-pane">
                        <div class="row equl">
                            <div class="col-sm-12">
                                <strong style="text-align:center; font-size:28px; color:#fff;">Freshwater Boats</strong>
                            </div>
                        </div>
                    </div>
                    <div id="bank" class="tab-pane">
                        <div class="row equl">
                            <div class="col-sm-12">
                                <strong style="text-align:center; font-size:28px; color:#fff;">Bank Owned</strong>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <div class="adds-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-12">
                        <figure>
                            <img src="https://via.placeholder.com/705x223" alt="Adds">
                        </figure>
                    </div>
                    <div class="col-sm-6 col-12">
                        <figure>
                            <img src="https://via.placeholder.com/705x223" alt="Adds">
                        </figure>
                    </div>
                </div>
            </div>
        </div>

        <section class="welcome">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7 col-12">
                        <div class="welcome_note">

                            <h3><strong><?php the_field('about_us_title');?></strong></h3>
                            
                            <?php the_field('about_us_description');?>
                            <!-- <div class="outboard">
                                <p>Be sure to check out our NEW and industries ONLY direct links for:</p>
                                <ul>
                                    <li>Outboards Only </li>
                                    <li>Bank Blowouts </li>
                                    <li>Solely Center Console</li>
                                    <li>Freshwater Flotilla</li>
                                    <li>Antiques and Classics</li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-sm-5 col-12">
                        <div class="welcome_img">
                            <figure>
                                <?php $aboutImage = get_field('about_us_image');?>
                                <img src="<?php echo $aboutImage['url'];?>" alt="<?php echo $aboutImage['alt'];?>">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="promo">
            <div class="container">
                <?php the_field('about_us_content');?>
            </div>
        </div>
        <section class="countries">
            <div class="container">
                <h3>Brokered <strong>Countries</strong></h3>
                <div class="countries_content">
                    <div class="countries_effect">
                        <div class="box">
                            <figure style="background-image: url(images/countries-1.jpg)">
                                <div class="box_area">
                                    <h3>United States</h3>
                                </div>
                            </figure>
                            <div class="box-content">
                                <h3 class="title">United States</h3>
                                <a href="#">view All</a>
                            </div>
                        </div>
                        <div class="box">
                            <figure style="background-image: url(images/countries-2.jpg)">
                                <div class="box_area">
                                    <h3>New York</h3>
                                </div>
                            </figure>
                            <div class="box-content">
                                <h3 class="title">New York</h3>
                                <a href="#">view All</a>
                            </div>
                        </div>
                        <div class="box">
                            <figure style="background-image: url(images/countries-3.jpg)">
                                <div class="box_area">
                                    <h3>Florida</h3>
                                </div>
                            </figure>
                            <div class="box-content">
                                <h3 class="title">Florida</h3>
                                <a href="#">view All</a>
                            </div>
                        </div>
                        <div class="box">
                            <figure style="background-image: url(images/countries-4.jpg)">
                                <div class="box_area">
                                    <h3>California</h3>
                                </div>
                            </figure>
                            <div class="box-content">
                                <h3 class="title">California</h3>
                                <a href="#">view All</a>
                            </div>
                        </div>
                        <div class="box">
                            <figure style="background-image: url(images/countries-5.jpg)">
                                <div class="box_area">
                                    <h3>Maryland</h3>
                                </div>
                            </figure>
                            <div class="box-content">
                                <h3 class="title">Maryland</h3>
                                <a href="#">view All</a>
                            </div>
                        </div>
                    </div>
                    <div class="button">
                        <a class="btn-effect" href="#"><span>View All</span></a>
                    </div>
                </div>
            </div>
        </section>
        <div class="resume_send">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-12">
                        <div class="box_left">
                            <p>
                                Interested in working on the marine industry for Worldwide Yachtsman as a Field
                                Reporter,
                                Sales and Marketing, SEO,
                                Tradeshows, Social Media, Blogger, Editor or IT Professional?
                            </p>

                            <div class="button">
                                <a class="btn-effect resume_btn" href="#">Send us your resume</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-12">
                        <div class="box_left right">
                            <p>
                                Interested in advertising to the greatest demograpihic in the world?
                            </p>
                            <p>Prices Start at $39.95 per month</p>
                            <div class="button">
                                <a class="btn-effect resume_btn" href="#">Advertise your ads</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
 
 <?php get_footer();?>