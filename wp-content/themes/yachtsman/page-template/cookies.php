<?php 
/*
Template Name: cookies
*/
get_header();
get_sidebar();

?>
        <?php $bannerImage = get_field('cookies_banner_image');?>

        <section class="inner banner" style="background-image: url(<?php echo $bannerImage['url'];?>)">

            <div class="container">
                <div class="banner_content">
                    <h2><?php the_field('cookies_banner_text');?></h2>
                </div>
            </div>
        </section>
        <section class="cookies">
            <div class="container">

                <?php the_field('cookies_content');?>

                    <?php if( have_rows('popular_browser') ): ?>

                        <ul>

                            <?php while( have_rows('popular_browser') ): the_row(); 

                                $image = get_sub_field('browser_image');
                                $title = get_sub_field('browser_name');
                               
                            ?>
                                <li>
                                    <figure>
                                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>">
                                    </figure>
                                    <p><?php echo $title; ?></p>
                                </li>
                            
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
            </div>
        </div>
    </section>

<?php get_footer();?>