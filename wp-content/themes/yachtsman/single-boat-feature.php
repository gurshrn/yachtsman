<?php 

get_header();
get_sidebar();

$featureBannerImage = get_field('features_banner_image','options');

 ?>

        <section class="inner banner" style="background-image: url(<?php echo $featureBannerImage['url'];?>)">
            
            <div class="container">
                <div class="banner_content">

                    <h2><?php echo the_field('features_banner_text');?></h2>

                </div>
            </div>
        </section>
        <section class="features details">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 col-12">
                        <div class="blog_detals">

                            <?php
                                $post_id = get_the_id();
                                $my_post = get_post($post_id);
                                $post_date = $my_post->post_date;    
                                $post_content = $my_post->post_content;         
                                $post_title = $my_post->post_title;     
                                $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') );
                                $imageGallery = get_field('gallery'); 
                        
                            ?>

                            <figure style="background-image: url(<?php echo $image;?>)"></figure>
                            <h4><?php echo $post_title;?></h4>
                            <span><?php echo date('M d, Y',strtotime($post_date));?></span>
                            
                            <p><?php echo $post_content;?></p>
                            
                            <div class="features_social">
                                <ul>
                                    <li> <a href="#"><i class="fa fa-facebook" aria-hidden="true" target="blank"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true" target="blank"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true" target="blank"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true" target="blank"></i></a></li>
                                    <li><a href="#"><i class="fa fa-tumblr" aria-hidden="true" target="blank"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-12">
                        <div class="blog_right">
                            <form>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="search" placeholder="Search...">
                                    <i class="fa fa-search"></i>
                                </div>
                            </form>
                            <div class="recent_post">
                                <h4>Recent Articles</h4>

                                <?php
                                    $args = array(
                                      'post_type'   => 'boat-feature',
                                      'post_status' => 'publish',
                                      'posts_per_page' => 5,
                                      'order' => 'DESC',
                                    );
                                    $products = new WP_Query( $args );
                                    
                                    if( $products->have_posts() ) :
                                        
                                        while( $products->have_posts() ) :
                                            
                                            $products->the_post();

                                ?>
                                        <div class="post">
                                            <figure>
                                                <img src="<?php echo the_post_thumbnail_url();?>" alt="Post">
                                            </figure>
                                            <div class="post_head">
                                                <a href="<?php the_permalink();?>"><?php echo  get_the_title(); ?></a>
                                                <p>
                                                    <?php 
                                                        
                                                        $trimexcerpt = get_the_excerpt();
                                                        
                                                        $shortexcerpt = wp_trim_words( $trimexcerpt, $num_words = 10, $more = '… ' );
                                                        
                                                        echo $shortexcerpt;

                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                <?php 

                                    endwhile;
                                    wp_reset_postdata();
                                    endif;
                                ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php get_footer();?>