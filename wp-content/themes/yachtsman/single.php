<?php 

get_header();
get_sidebar();

if($post->post_type == 'post')
{

?>

    <?php $bannerImage = get_field('blog_banner_image','options');?>

    <section class="inner banner" style="background-image: url(<?php echo $bannerImage['url'];?>)">
        <div class="container">
            <div class="banner_content">

                <h2><?php the_field('blog_banner_text','options');?></h2>

            </div>
        </div>
    </section>
        <section class="blog inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 col-12">
                        <div class="blog_left">
                            
                            <div class="blog_img">
                                <div class="blog_bottom">
                                    <div class="year">
                                        <p><strong>01</strong>Sept</p>
                                    </div>
                                    <div class="bottom_right">
                                        <h4>Merica!</h4>
                                        <p>
                                            I’m going to preface by saying I love Sea Ray boats, always have and always
                                            will. It started with my first sexy gal the
                                            1990 Sea Ray Laguna and went through most models up to my current marriage
                                            with the 50 Express Luxury Cruisers.
                                        </p>
                                        <p>
                                            Most of us know in approx June of 2018 Brunswick Marine has decided to
                                            cease production of all Sea Ray Yachts and Sea
                                            Ray Sport Yachts. Production of vessels over 40 foot would wind down in 3rd
                                            quarter and be discontinue in 4th quarter
                                            of 2018. This move will cost almost 900 jobs. Sea Ray was always a good
                                            value and carried above industry standards in
                                            residual resale because of it’s cult following and fundamentally solid
                                            building methods. This chain of events caught my
                                            attention.
                                        </p>
                                        <p>
                                            I’ve began pondering how this could happen in what most believe is a robust
                                            economy with lessening environmental
                                            regulations for major manufactures. I know the company CEO remarked the
                                            need to eliminate was because of weak financial
                                            performance of the yacht product category, but I ain’t picking up with he’s
                                            throwing around. I blame the new modern
                                            (some call it futuristic) appearance of the yacht line.
                                        </p>

                                    </div>
                                </div>
                                <figure style="background-image: url(images/blog_2.jpg)"></figure>
                                <p>
                                    Above is the 2018 Sea Ray 400 Sundancer.
                                </p>
                                <p>
                                    I looked at the 400 Sundancer at a show and asked myself who would ever want one.
                                    It fell WAY outside of the traditions
                                    set forth by the brand. It looks like a wedding cake that dropped off a table to
                                    me. I won’t even mention the ride,
                                    although the interior design and fit and finish was exceptional.
                                </p>
                                <figure style="background-image: url(images/blog-1.jpg)"></figure>
                                <p>
                                    Above is the 2009 Sea Ray 55 Sundancer
                                </p>
                                <p>
                                    Above is what I believe is the pinnacle of the Sea Ray designs. These classic lines
                                    are timeless and the 55 is easy to
                                    pilot and maintain. Doesn’t it look like 30 years of productions between the two
                                    models?
                                </p>
                                <p>
                                    I believe Brunswick Marine threw the big boats out with the bath water. I’m going
                                    to miss seeing the majestic Sea Ray
                                    Sport Yacht prowling the waters of the world. I hope someone buys this brand and
                                    continues the tradition they invented
                                    with big and fun express cruisers. To me they screamed out Merica!
                                </p>
                                <div class="features_social">
                                    <ul>
                                        <li> <a href="#"><i class="fa fa-facebook" aria-hidden="true" target="blank"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true" target="blank"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true" target="blank"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-12">
                        <div class="blog_right">
                            <form>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="search" placeholder="Search...">
                                    <i class="fa fa-search"></i>
                                </div>
                            </form>
                            <div class="recent_post">
                                <h4>Recent Articles</h4>
                                <ul>
                                    <li>May there always be water under your boat,</li>
                                    <li>May she always be seaworthy,ever afloat, </li>
                                    <li> May the bilge pump be certain to worknight and day, </li>
                                    <li>May the compass and charts always show the safe way, </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!-- Feature Detail -->

<?php } else if($post->post_type == 'feature') { ?>

    <?php $featureBannerImage = get_field('feature_banner_image','options');?>

        <section class="inner banner" style="background-image: url(<?php echo $featureBannerImage['url'];?>)">

            <div class="container">
                <div class="banner_content">

                    <h2><?php the_field('feature_banner_text');?></h2>

                </div>
            </div>
        </section>
        <section class="features details">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 col-12">
                        <div class="blog_detals">
                            <figure style="background-image: url(images/blog-details-1.jpg)"></figure>
                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                            <span>September 16, 2018</span>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim,
                                elementum ut interdum ut, tempor ac
                                lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed
                                odio condimentum vestibulum sit
                                amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue. Vivamus
                                hendrerit, lacus sit amet dignissim
                                venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit. Donec
                                aliquam lobortis rhoncus. Integer ut
                                vestibulum justo. Sed volutpat nulla libero, sed vehicula neque condimentum et. Nulla
                                est risus, mollis ac risus ac,
                                luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac,
                                dignissim at nisi. Fusce
                                laoreet dolor ac fermentum cursus. Donec ultricies dolor faucibus aliquet posuere.
                                Suspendisse volutpat purus eget
                                dictum ultricies. Curabitur nec tortor volutpat lorem cursus tempus. Sed efficitur
                                felis in mauris posuere viverra. Ut
                                dolor neque, faucibus id tempus eu, sagittis eget tellus.
                            </p>
                            <p>
                                Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula,
                                in aliquet erat tortor sed
                                velit. Donec aliquam lobortis rhoncus. Integer ut vestibulum justo. Sed volutpat nulla
                                libero, sed vehicula neque
                                condimentum et. Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed
                                velit magna. Nulla erat quam,
                                auctor vitae volutpat ac Nulla est risus, mollis ac risus ac, luctus lacinia tortor.
                                Nunc sed velit magna. Nulla erat
                                quam, auctor vitae volutpat ac, dignissim at nisi. Fusce laoreet dolor ac fermentum
                                cursus. Donec ultricies dolor
                                faucibus aliquet posuere.
                            </p>
                            <p>
                                Suspendisse volutpat purus eget dictum ultricies. Curabitur nec tortor volutpat lorem
                                cursus tempus. Sed efficitur
                                felis in mauris posuere viverra. Ut dolor neque, faucibus id tempus eu, sagittis eget
                                tellus.
                            </p>
                            <div class="features_social">
                                <ul>
                                    <li> <a href="#"><i class="fa fa-facebook" aria-hidden="true" target="blank"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true" target="blank"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true" target="blank"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true" target="blank"></i></a></li>
                                    <li><a href="#"><i class="fa fa-tumblr" aria-hidden="true" target="blank"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-12">
                        <div class="blog_right">
                            <form>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="search" placeholder="Search...">
                                    <i class="fa fa-search"></i>
                                </div>
                            </form>
                            <div class="recent_post">
                                <h4>Recent Articles</h4>
                                <div class="post">
                                    <figure>
                                        <img src="images/post-1.jpg" alt="Post">
                                    </figure>
                                    <div class="post_head">
                                        <a href="#">Lorem ipsum dolor</a>
                                        <p>
                                            Lorem ipsum dolor sit amet, adipiscing sit elit.
                                        </p>
                                    </div>
                                </div>
                                <div class="post">
                                    <figure>
                                        <img src="images/post-2.jpg" alt="Post">
                                    </figure>
                                    <div class="post_head">
                                        <a href="#">Lorem ipsum dolor</a>
                                        <p>
                                            Lorem ipsum dolor sit amet, adipiscing sit elit.
                                        </p>
                                    </div>
                                </div>
                                <div class="post">
                                    <figure>
                                        <img src="images/post-3.jpg" alt="Post">
                                    </figure>
                                    <div class="post_head">
                                        <a href="#">Lorem ipsum dolor</a>
                                        <p>
                                            Lorem ipsum dolor sit amet, adipiscing sit elit.
                                        </p>
                                    </div>
                                </div>
                                <div class="post">
                                    <figure>
                                        <img src="images/post-4.jpg" alt="Post">
                                    </figure>
                                    <div class="post_head">
                                        <a href="#">Lorem ipsum dolor</a>
                                        <p>
                                            Lorem ipsum dolor sit amet, adipiscing sit elit.
                                        </p>
                                    </div>
                                </div>
                                <div class="post">
                                    <figure>
                                        <img src="images/post-4.jpg" alt="Post">
                                    </figure>
                                    <div class="post_head">
                                        <a href="#">Lorem ipsum dolor</a>
                                        <p>
                                            Lorem ipsum dolor sit amet, adipiscing sit elit.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php } else{ ?>

<!-- Product Detail -->

<?php $productBannerImage = get_field('product_banner_image','options');?>

 <section class="inner banner" style="background-image: url(<?php echo $productBannerImage['url'];?>)">
            <div class="container">
                <div class="banner_content">
                    <h2><?php echo the_field('product_banner_text','options');?></h2>
                </div>
            </div>
        </section>
        <section class="product_detals">
            <div class="container">
                <div class="row">

                    <?php
                        $post_id = get_the_id();
                        $my_post = get_post($post_id);
                        $post_content = $my_post->post_content;         
                        $post_title = $my_post->post_title;     
                        $post_country = $my_post->post_country;     
                        $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') );
                        $imageGallery = get_field('gallery'); 
                
                    ?>

                    <div class="col-sm-9 col-12">
                        <div class="product_detail_content">
                            <div class="details_head">
                                <div class="top_head">
                                    <h4><?php echo  $post_title; ?></h4>
                                    <p><?php echo $post_country;?></p>
                                </div>
                                <div class="top_head right">
                                    <h4>US$ 264,500*</h4>
                                    <p>$1157.19/month</p>
                                </div>
                            </div>
                            <div class="product_slide">
                                <div class="slider">
                                    <div id="product" class="flexslider">
                                        <ul class="slides">
                                            <?php

                                                if( $imageGallery ): 
                                                    foreach( $imageGallery as $gallery ): 
                                            ?>

                                                    <li>
                                                        <figure style="background-image:url(<?php echo $gallery['url'] ; ?>)"></figure>
                                                    </li>

                                            <?php 
                                                endforeach;
                                                endif; 

                                            ?>

                                        </ul>
                                    </div>
                                    <div id="product_details" class="flexslider">
                                        <ul class="slides">
                                            <?php

                                                if( $imageGallery ): 
                                                    foreach( $imageGallery as $gallery ): 
                                            ?>

                                                    <li>
                                                        <figure style="background-image:url(<?php echo $gallery['url'] ; ?>)"></figure>
                                                    </li>

                                            <?php 
                                                endforeach;
                                                endif; 

                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="product_call">
                                <h5>Interested in this boat?</h5>
                                <div class="button">
                                    <a class="btn-effect" href="#"><span>Send Email</span></a>
                                </div>
                                <div class="button">
                                    <a class="btn-effect active" href="#"><span>Call Now</span></a>
                                </div>
                            </div>
                            <div class="product_tabs">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#description">Description</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#full-secifications">View Full
                                            Specifications</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div id="description" class="tab-pane active description">
                                        <h3>Nuovo arrivo</h3>
                                        <ul>
                                            <li><strong>Year</strong>2000</li>
                                            <li><strong>Length</strong>1000’</li>
                                            <li><strong>Engine/ Fuel Type</strong>/</li>
                                            <li><strong>Located In:</strong>Miami, FLt</li>
                                            <li><strong>Hull Material:</strong>Steel</li>
                                            <li><strong>YW#:</strong>79415-3241578</li>
                                        </ul>
                                        <h5><strong>Current Price: </strong> USD 230,000 Tax Paid (US$ 264,500)</h5>

                                        <?php the_field('boat_description');?>

                                        <?php the_field('boat_contact_detail');?>
                                    
                                    </div>
                                    <!--Tab-panes-2 -->
                                    <div id="full-secifications" class="tab-pane fade secifications">
                                        <?php the_field('boat_specification');?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-12">
                        <div class="product_details_ads">
                            <figure>
                                <span class="thumbnail_wrap image">
                                    <span><img alt="" class="thumbnail" data-attachment-id="347427814" data-audio-codec="null"
                                            data-classes="image" data-container-id="comment_647386854"
                                            data-content-type="image/gif" data-created-at="2018-09-10T14:00:08Z"
                                            data-creator-id="16742695" data-creator="John R." data-description="image"
                                            data-details-path="/2175374/projects/15787039/attachments/347427814/details"
                                            data-download-path="/2175374/projects/15787039/attachments/347427814/download"
                                            data-embeddable="false" data-extension="GIF" data-file-or-image="image"
                                            data-filename="WWY-Margin Ad for Adwords Remarketing.gif" data-filesize="153 KB"
                                            data-height="280" data-image-id="347427814" data-large-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/large.gif"
                                            data-linked="null" data-max-size="700" data-original-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814"
                                            data-path="/2175374/projects/15787039/attachments/347427814"
                                            data-perma-path="/2175374/projects/15787039/messages/80428610?enlarge=347427814#attachment_347427814"
                                            data-previewable="true" data-storage-key="b2d42efe-b501-11e8-bb59-047d7badf251"
                                            data-thumbnail="true" data-trash-path="/2175374/projects/15787039/attachments/347427814/trash"
                                            data-trashed="false" data-type="image" data-video-codec="null" data-width="336"
                                            src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/thumbnail.gif"
                                            data-scaled="true" style="width: 336px; height: 280px;"></span>
                                </span>
                            </figure>
                            <figure>
                                <span class="thumbnail_wrap image">
                                    <span><img alt="" class="thumbnail" data-attachment-id="347427814" data-audio-codec="null"
                                            data-classes="image" data-container-id="comment_647386854"
                                            data-content-type="image/gif" data-created-at="2018-09-10T14:00:08Z"
                                            data-creator-id="16742695" data-creator="John R." data-description="image"
                                            data-details-path="/2175374/projects/15787039/attachments/347427814/details"
                                            data-download-path="/2175374/projects/15787039/attachments/347427814/download"
                                            data-embeddable="false" data-extension="GIF" data-file-or-image="image"
                                            data-filename="WWY-Margin Ad for Adwords Remarketing.gif" data-filesize="153 KB"
                                            data-height="280" data-image-id="347427814" data-large-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/large.gif"
                                            data-linked="null" data-max-size="700" data-original-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814"
                                            data-path="/2175374/projects/15787039/attachments/347427814"
                                            data-perma-path="/2175374/projects/15787039/messages/80428610?enlarge=347427814#attachment_347427814"
                                            data-previewable="true" data-storage-key="b2d42efe-b501-11e8-bb59-047d7badf251"
                                            data-thumbnail="true" data-trash-path="/2175374/projects/15787039/attachments/347427814/trash"
                                            data-trashed="false" data-type="image" data-video-codec="null" data-width="336"
                                            src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/thumbnail.gif"
                                            data-scaled="true" style="width: 336px; height: 280px;"></span>
                                </span>
                            </figure>
                            <figure>
                                <span class="thumbnail_wrap image">
                                    <span><img alt="" class="thumbnail" data-attachment-id="347427814" data-audio-codec="null"
                                            data-classes="image" data-container-id="comment_647386854"
                                            data-content-type="image/gif" data-created-at="2018-09-10T14:00:08Z"
                                            data-creator-id="16742695" data-creator="John R." data-description="image"
                                            data-details-path="/2175374/projects/15787039/attachments/347427814/details"
                                            data-download-path="/2175374/projects/15787039/attachments/347427814/download"
                                            data-embeddable="false" data-extension="GIF" data-file-or-image="image"
                                            data-filename="WWY-Margin Ad for Adwords Remarketing.gif" data-filesize="153 KB"
                                            data-height="280" data-image-id="347427814" data-large-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/large.gif"
                                            data-linked="null" data-max-size="700" data-original-src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814"
                                            data-path="/2175374/projects/15787039/attachments/347427814"
                                            data-perma-path="/2175374/projects/15787039/messages/80428610?enlarge=347427814#attachment_347427814"
                                            data-previewable="true" data-storage-key="b2d42efe-b501-11e8-bb59-047d7badf251"
                                            data-thumbnail="true" data-trash-path="/2175374/projects/15787039/attachments/347427814/trash"
                                            data-trashed="false" data-type="image" data-video-codec="null" data-width="336"
                                            src="https://asset1.basecamp.com/2175374/projects/15787039/attachments/347427814/b2d42efe-b501-11e8-bb59-047d7badf251/thumbnail.gif"
                                            data-scaled="true" style="width: 336px; height: 280px;"></span>
                                </span>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php } ?>

<?php get_footer(); ?>
