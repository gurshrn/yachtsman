<header id="header-sroll">
	<div class="container">
		<div class="header-top">
			<div class="logo">
			
				<?php $mainImage = get_field('header_logo','options');?>
				
				<a href="<?php echo get_site_url();?>">
					<img src="<?php echo $mainImage['url'];?>" alt="logo">
				</a>
			</div>
			<nav class="navbar navbar-expand-lg">
				<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
					<span class="navbar-toggler-icon"></span>
				</button>
				<!-- Navbar links -->
				<div class="collapse navbar-collapse" id="collapsibleNavbar">

					<?php $menu = wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'navbar-nav') ); ?>
					
					<!-- <ul id="menu-primary-menu" class="nav navbar-nav">
						<li><a href="index.html">Boats</a></li>
						<li><a href="features.html">Features</a></li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
								href="services.html">Services</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="worldwide-cacht-charters.html">Worldwide Yacht Charters</a></li>
							</ul>
						</li>
						<li class=""><a href="membership.html">Membership</a></li>
						<li class=""><a href="#">Brokers</a></li>
						<li class=""><a href="#">Contact Us</a></li>
					</ul> -->
				</div>
			</nav>
			<div class="login_area">
				<div class="top_head">
					<?php the_field('header_content','options');?>
				</div>
				<div class="country">
					<ul>
						<li>
							<a href="#" class="login" data-toggle="modal" data-target="#login">Login /
								Sign Up</a>
						</li>
						<li class="flag">
							<span>
								<img src="<?php echo get_template_directory_uri()?>/images/flag.png" alt="flag">
								<div class="form-group">
									<select class="form-control">
										<option>United States</option>
										<option>United States</option>
										<option>English (asd)</option>
									</select>
								</div>
							</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>
