<?php 

$model_key = "field_5c3f079be0682"; 
$model = get_field_object($model_key); 

$condition_key = "field_5c3f098edf76f"; 
$condition = get_field_object($condition_key);

$boat_type_key = "field_5c3f08d3ada86"; 
$boattype = get_field_object($boat_type_key);

$length_type_key = "field_5c3f0a763166b"; 
$lengthtype = get_field_object($length_type_key);

$price_key = "field_5c3f09ed1cdb1"; 
$price = get_field_object($price_key);

//echo "<pre>"; print_r($lengthtype);die;
?>

<section class="boat_sale wow fadeIn" data-wow-delay="0.3s">
    <div class="container">
        <div class="boat_content">
            <h3>Boats For Sale</h3>
            
            <form action="<?php echo get_site_url().'/products/';?>" method="post">
                <div class="manufacturer">
                    <div class="select_modal">
                        <div class="form-group">
                            <label><?php echo $model['label'];?></label>
                            <input class="form-control" type="text" name="<?php echo $model['name'];?>">
                        </div>
                    </div>
                    <div class="new_type">
                        <div class="form-group">
                            <label>New/Used</label>

                             
                                <p> 
                                    <input type="radio" id="rd_0" name="<?php echo $condition['name'];?>" value="all" checked>
                                    <label for="rd_0">All</label>
                                </p>

                                <?php
                                    $i=1; 
                                    foreach($condition['choices'] as $key=>$val)
                                    {
                                        
                                        echo '<p> 
                                                <input type="radio" id="rd_'.$i.'" name="'.$condition['name'].'" value="'.$key.'">
                                                <label for="rd_'.$i.'">'.$val.'</label>
                                            </p>';
                                        $i++;
                                    }

                                ?>
                        </div>
                    </div>
                    <div class="used_type">
                        <div class="form-group">
                            <label><?php echo $boattype['label'];?></label>

                                <p> 
                                    <input type="radio" id="bt_0" name="<?php echo $boattype['name'];?>" value="all" checked>
                                    <label for="bt_0">All</label>
                                </p>

                                <?php
                                    $i=1; 
                                    foreach($boattype['choices'] as $key=>$vals)
                                    {
                                        echo '<p> 
                                                <input type="radio" id="bt_'.$i.'" name="'.$boattype['name'].'" value="'.$key.'">
                                                <label for="bt_'.$i.'">'.$vals.'</label>
                                            </p>';
                                        $i++;
                                    }

                                ?>
                        </div>
                    </div>
                    <div class="lenght">
                        <div class="form-group">
                            <label>Length</label>

                            <p> 
                                <input type="radio" id="lt1" name="length_type" value="ft">
                                <label for="lt1">FT</label>
                            </p>

                            <p> 
                                <input type="radio" id="lt2" name="length_type" value="m">
                                <label for="lt2">M</label>
                            </p>
                                    
                            
                            <input class="form-control" type="text" name="to">
                            <span>To</span>
                            <input class="form-control" type="text" name="from">
                        </div>
                    </div>
                </div>
                <div class="year_modal">
                            <div class="rework">
                                <div class="rework_area">
                                    <div class="form-group">
                                        <label>Year</label>
                                        <select class="form-control">
                                            <option value="">Select Year...</option>
                                            <?php 
                                                $firstYear = (int)date('Y') - 84;
                                                $lastYear = date('Y');
                                                for($i=$firstYear;$i<=$lastYear;$i++)
                                                {
                                                    echo '<option value='.$i.'>'.$i.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <span>To</span>
                                <div class="rework_area">
                                    <div class="form-group">
                                        <label></label>
                                        <select class="form-control">
                                            <option value="">Select Year...</option>
                                            <?php 
                                                $firstYear = (int)date('Y') - 84;
                                                $lastYear = date('Y');
                                                for($i=$firstYear;$i<=$lastYear;$i++)
                                                {
                                                    echo '<option value='.$i.'>'.$i.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <div class="form-group">
                                    <label>Price USD</label>
                                    <select class="form-control">
                                        <option>Select Price Range</option>
                                        <option>Select Price Range</option>
                                        <option>Select Price Range</option>
                                    </select>
                                    <button class="boat_btn" type="submit">Search</button>
                                </div>
                            </div>
                            <div class="advance_search">
                                <a href="<?php echo get_site_url().'/advance-search';?>">Advanced Search</a>
                            </div>
                        </div>
            </form>
        </div>
    </div>
</section>