<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
	<!-- Footer -->
        <footer>
            <div class="container">
                <div class="footer_main">
                    <div class="footer_logo">
					
						<?php $footerLogo = get_field('footer_logo','options');?>
						
                        <a href="<?php echo get_site_url();?>">
                            <img src="<?php echo $footerLogo['url'];?>" alt="Footer-Logo">
                        </a>
                        <div class="social">

                            <h4><?php the_field('connect_us_title','options');?></h4>

                            <ul>
                                <li> <a href="<?php the_field('facebook_link','options');?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                                <li><a href="<?php the_field('twitter_link','options');?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                                <li><a href="<?php the_field('tumblr_link','options');?>" target="_blank"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>

                                <li><a href="<?php the_field('linkedin_link','options');?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                                <li><a href="<?php the_field('instagram_link','options');?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

                                <li><a href="<?php the_field('youtube_link','options');?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>

                                <li><a href="<?php the_field('pinterest_link','options');?>" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="link">
                        <h4>Quick Links</h4>

                        <?php $menu = wp_nav_menu( array( 'theme_location' => 'footer') ); ?>
                       
                    </div>
                    <div class="contact_us">

                        <h4><?php the_field('contact_us','options');?></h4>

                        <ul>
                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <a href="tel:<?php the_field('phone_number','options');?>"><?php the_field('phone_number','options');?></a>
                            </li>
                            <li>
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <a href="mailto:<?php the_field('email','options');?>"><?php the_field('email','options');?></a>
                            </li>
                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <p><?php the_field('address','options');?></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">

                    <p><?php the_field('copyright_text','options');?></p>

                    <span>Powered By - <a href="https://www.imarkinfotech.com/">iMark Infotech</a></span>
                </div>
            </div>
            <!-- Live-Chat-Image -->
            <div class="live_img">
                <img src="images/live.png" alt="Live">
            </div>
        </footer>


        <!-- The Modal -->
        <div class="modal  login_popup" id="login">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4>Login</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <img src="images/close.png" alt="close">
                        </button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="login">

                            <form  action="<?php echo get_site_url().'/login-page/'; ?>" method="post"> 

                                <input class="form-control" type="hidden" name="login" value="login">

                                <div class="form-group">
                                    <input class="form-control" type="email" name="email" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="password" name="password" placeholder="Password" required>
                                </div>
                            
                            <div class="rembember">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="rembember"><span class="new-checkbox"></span> Remember me</label>
                                </div>
                                <a href="#" data-toggle="modal" data-target="#forget" data-dismiss="modal">Forget
                                    Password</a>
                            </div>
                            <div class="submit">
                                <button type="submit">Login</button>
                            </div>
                            </form>
                            <div class="not_registration">
                                <p>
                                    Not registered?
                                    <a href="#" data-toggle="modal" data-target="#sign_up" data-dismiss="modal">Sign Up
                                        Now</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Sign-UP -->
        <div class="modal login_popup" id="sign_up">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4>Sign Up</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <img src="images/close.png" alt="close">
                        </button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="login">

                            <form id="login1" name="form" action="<?php echo get_site_url().'/register/'; ?>" method="post"> 

                                <input class="form-control" type="hidden" name="register" value="register">

                                <div class="form-group">
                                    <input class="form-control" id="email" type="email" name="email" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="password" type="password" name="password" placeholder="Password" required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="number" type="text" name="number" placeholder="Number">
                                </div>
                            
                            <div class="submit">
                                <button type="submit">Submit</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Forget-Password -->
        <div class="modal login_popup" id="forget">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4>FORGOT PASSWORD</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <img src="images/close.png" alt="close">
                        </button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="login">
                            <form>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="mail" placeholder="Email">
                                </div>
                            
                            <div class="submit">
                                <button type="submit">Submit</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
<?php echo wp_footer();?>

        <!-- jQuery first, then Bootstrap JS. -->
        <script src="<?php echo get_template_directory_uri()?>/js/jquery3.3.1.min.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/js/popper.min.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/js/bootstrap.min.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/js/parallax.min.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/js/wow.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/js/owl.carousel.min.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/js/jquery.flexslider.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/js/custom.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/js/ajax-auth-script.js"></script>
    </body>

</html>

	
