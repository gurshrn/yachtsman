<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Worldwideyachtsman</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri()?>/images/favicon.png" sizes="32x32" type="image/x-icon">

        <!-- CSS -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/css/main.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/css/dashboard.css">

       
    </head>

    <body>



	

