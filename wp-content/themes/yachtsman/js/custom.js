jQuery(document).ready(function () {
       (function ($) {
        'use strict';
        jQuery('#carouselFade').carousel();
        setHeader();
        //Header fix
        jQuery(window).scroll(function () {
            setHeader();
        });
        function setHeader() {
            var head = jQuery("#header-sroll");
            var windowpos = jQuery(window).scrollTop();
            if (windowpos >= 50) {
                head.addClass("fixed-header");
            } else {
                head.removeClass("fixed-header");
            }
        }
        // bootstrap-Dropdown
           jQuery('ul.nav li.dropdown').hover(function () {
               jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
           }, function () {
               jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
           });
        // Input FirstLetter Cap
        jQuery('.firstCap, textarea').on('keypress', function (event) {
            var jQuerythis = jQuery(this),
                thisVal = jQuerythis.val(),
                FLC = thisVal.slice(0, 1).toUpperCase(),
                con = thisVal.slice(1, thisVal.length);
            jQuery(this).val(FLC + con);
        });

        // Listing Slider
        var listingSlider = jQuery('#listingSlider');
        if (listingSlider.length > 0) {
            listingSlider.owlCarousel({
                loop: true,
                margin: 32,
                items: 3,
                nav: false,
                dots: true
            })
        }

        /* Wow Animation*/
        new WOW().init();

        //Avoid pinch zoom on iOS
        document.addEventListener('touchmove', function (event) {
            if (event.scale !== 1) {
                event.preventDefault();
            }
        }, false);
    })(jQuery)
});

// Date-Picker
$(function () {
    jQuery("#datepicker, #datepicker-2").datepicker({
        autoclose: true,
        todayHighlight: true
    }).datepicker('update', new Date());
});

jQuery(window).on('load', function () {

    //Preloader
    var preloader = jQuery('.preloader');
    setTimeout(function () {
        preloader.hide();
        jQuery('.ajaxpreloader').hide();
    }, 2000);
})

// Flex-Slider
jQuery('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 262,
    asNavFor: '#slider'
});
jQuery('#product_details').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 200,
    itemMargin: 10,
    asNavFor: '#product'
});
jQuery('#slider, #product').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel",
    start: function (slider) {
        jQuery('body').removeClass('loading');
    }
});
// --FLex-Slider
// Scolling-IE
if (navigator.userAgent.match(/Trident\/7\./)) { // if IE
    jQuery('body').on("mousewheel", function () {
        // remove default behavior
        event.preventDefault();

        //scroll without smoothing
        var wheelDelta = event.wheelDelta;
        var currentScrollPosition = window.pageYOffset;
        window.scrollTo(0, currentScrollPosition - wheelDelta);
    });
}

    // Banner-Background-Image-Zoom-For-all-Browsers
if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1) {
    jQuery(window).scroll(function () {
        var scroll = $(window).scrollTop();
        jQuery(".banner ").css({
            backgroundSize: (100 + scroll / 15) + "%",
            top: -(scroll / 30) + "%",
        });
    });
}
else if (navigator.userAgent.indexOf("Chrome") != -1) {
    jQuery(window).scroll(function () {
        var scroll = $(window).scrollTop();
        jQuery(".banner ").css({
            backgroundSize: (100 + scroll / 15) + "%",
            top: -(scroll / 30) + "%",
        });
    });
}
else if (navigator.userAgent.indexOf("Safari") != -1) {
    jQuery(window).scroll(function () {
        var scroll = $(window).scrollTop();
        jQuery(".banner ").css({
            backgroundSize: (100 + scroll / 15) + "%",
            top: -(scroll / 30) + "%",
        });
    });
}
else if (navigator.userAgent.indexOf("Firefox") != -1) {
}
else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
    jQuery(window).scroll(function () {
        var scroll = $(window).scrollTop();
        jQuery(".banner ").css({
            backgroundSize: (100 + scroll / 15) + "%",
            top: -(scroll / 30) + "%",
        });
    });
}

// File-Upload-JQuery
jQuery('#fileInput').on('change', function () {
    var control = $('#inputFileMaskText');
    control.val(this.files[0].name);
});

jQuery('#paypalform').submit();