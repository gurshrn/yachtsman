<?php
/*
Plugin Name: Orders 
Description: Orders
Author: Gurjeevan
*/

if (is_admin())
{   
	function form_create_survey_result() 
	{  
		add_menu_page("Orders","Orders",'manage_options',"orders","");
		add_submenu_page("orders","ORDERS","ORDERS",'manage_options',"orders","mainPageCall");
		
	}  
	add_action('admin_menu','form_create_survey_result'); 
	

}

function mainPageCall()
{
	require ABSPATH.'wp-content/plugins/orders/templates/view_AllOrders.php';
} 
 

function funGlobalSetting()
{
	global $wpdb;
	$plugin_url = plugin_dir_url( __FILE__ );
	?>
	<link rel='stylesheet' href='<?php echo $plugin_url; ?>css/style.css' type='text/css'/>
	<link rel='stylesheet' href='<?php echo $plugin_url; ?>css/bootstrap.min.css' type='text/css'/>
	<link rel='stylesheet' href='https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css' type='text/css'/>
	<script src="<?php echo $plugin_url; ?>js/bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo $plugin_url; ?>js/jquery.validate.js"></script>
	<script src="<?php echo $plugin_url; ?>js/form.js"></script>
	<script src="<?php echo $plugin_url; ?>js/custom.js"></script>
<?php
}