<?php 
	funGlobalSetting();
	//echo 'view All USER AS PER SERVEY';
	global $wpdb,$post;
	
		if(isset($_GET['order_id']) && $_GET['order_id'] != "")
		{
			$orderId=$_GET['order_id'];
			
		?>
		<div class="box-container">
			<div class="bx-innr">
				
				<h1>CUSTOMER ORDER DETAILS</h1>
				
				<?php 
					$products  					=		unserialize(manageOrderMeta('get','products','',$orderId));
					$billing_first_name  		=		manageOrderMeta('get','billing_first_name','',$orderId);
					$billing_last_name  		=		manageOrderMeta('get','billing_last_name','',$orderId);
					$country  					=		manageOrderMeta('get','country','',$orderId);
					$state  					=		manageOrderMeta('get','state','',$orderId);
					$city	  					=		manageOrderMeta('get','city','',$orderId);
					$billing_address			=		manageOrderMeta('get','billing_address','',$orderId);
					$billing_zipcode			=		manageOrderMeta('get','billing_zipcode','',$orderId);
					$billing_phone				=		manageOrderMeta('get','billing_phone','',$orderId);
					$billing_email				=		manageOrderMeta('get','billing_email','',$orderId);
					
					
					$country	 				=		get_value_from_table('country','country',"	WHERE id='".$country."'  ");	
					$state		 				=		get_value_from_table('region','region',"	WHERE id='".$state."'  ");	
					$city		 				=		get_value_from_table('city','city',"	WHERE id='".$city."'  ");	
					
					$totalPrice 				=		get_value_from_table('wp_orders','totalCost',"	WHERE id='".$orderId."'  ");	
					$creation_timestamp 		=		get_value_from_table('wp_orders','creation_timestamp',"	WHERE id='".$orderId."'  ");	
					
					?>
					<table class="table clsOrderDetail" cellspacing="0" width="100%">
							<tr>
								<td>
									<div class="clsTblHeading">First Name</div> 
									<?php echo $billing_first_name;?>   </td>
								<td>
									<div class="clsTblHeading">Last Name</div>    
									<?php echo $billing_last_name;?>   </td>
							</tr>
							<tr>
								<td>
									<div class="clsTblHeading">Country</div> 		
									<?php echo $country;?>   </td>
								<td>
									<div class="clsTblHeading">State / City</div>	
									<?php echo $state.' / '.$city;?>   
								</td>
							</tr>
							<tr>
								<td>
									<div class="clsTblHeading">Phone</div> 				
									<?php echo $billing_phone;?>   
								</td>
								<td>
									<div class="clsTblHeading">Email</div> 				
									<?php echo $billing_email;?>  
								</td>
							</tr>
							<tr>
								<td>
									<div class="clsTblHeading">Zip Code</div> 			
									<?php echo $billing_zipcode;?>   
								</td>
								<td>
									<div class="clsTblHeading">Billing Date</div> 		
									<?php echo date('d-m-Y', $creation_timestamp);?>   
								</td>
							</tr>
							<tr>
								<td>
									<div class="clsTblHeading">Billing Address</div> 		
									<?php echo $billing_address;?>   
								</td>
								<td>
									<div class="clsTblHeading">Billing Products</div> 	
									<?php foreach ($products as $productIDs) :?>
										<li>
											<?php echo get_the_title($productIDs);?>
										</li>
									<?php endforeach;?>
								</td>
							</tr>
						
					</table>
				
			</div>
		</div>
		<?php
		}//end if 
		else
		{
			require ABSPATH.'wp-content/plugins/orders/templates/view_AllOrders.php';
		}
?>
