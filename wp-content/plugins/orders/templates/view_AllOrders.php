<?php 
funGlobalSetting();
//echo 'view ALl SERVEY';

if(isset($_GET['order_id']) && $_GET['order_id'] != "")
{
	require ABSPATH.'wp-content/plugins/orders/templates/view_users_as_per_order.php';
}
else{
?>
<div class="box-container">
	<div class="bx-innr">
		<h1>ORDERS DETAILS</h1>
		<?php 
		global $wpdb;
		$orderResults = $wpdb->get_results("SELECT * FROM wp_orders"); 
		if(!empty($orderResults))	
		{
		?>
			<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>S.No</th>
						<th>Order ID</th>
						<th>Customer Name</th>
						<th>Order Amount</th>
						<th>Payement Status</th>
						<th>Order Date</th>
						<th>Action</th>
					</tr>
				</thead>
				<?php 
				$i=1;
				?>
				<tbody>
				<?php 
					foreach($orderResults as $orderResult)	 :
					
					$billing_first_name  		=		manageOrderMeta('get','billing_first_name','',$orderResult->id);
					$billing_last_name  		=		manageOrderMeta('get','billing_last_name','',$orderResult->id);
				?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $orderResult->id;?></td>
						<td><?php echo $billing_first_name.' '.$billing_last_name;?></td>
						<td><?php echo $orderResult->totalCost;?></td>
						<td><?php if($orderResult->payment_status==1){ echo 'Paid';} else { echo 'Pending'; } ?></td>
						<td><?php echo date('d-m-Y', $orderResult->creation_timestamp);?></td>
						<td><a href="?page=orders&order_id=<?php echo $orderResult->id;?>" id="btn_view_order" name="view_order" class="cls_view_user">View</a></td>
					</tr>
				<?php
				$i++;
				endforeach;?>	
				</tbody>
			</table>	
		<?php
		}
		else
		{
		?>
			<div class="container">
				<div  class="row">
					<div class="col-sm-12">
						<h3>NO ORDERS AVAILABLE</h3>
					</div>
				</div>
			</div>
		<?php
		}
		?>
			
	</div>
</div>
<?php 
	
	}
	?>


<script>

jQuery(document).ready(function() {
    jQuery('#example').DataTable();
} );

</script>